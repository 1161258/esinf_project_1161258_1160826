/*
* A collection of graph algorithms.
 */
package libs.mapgraph;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author DEI-ESINF
 */
public class GraphAlgorithms {

    /**
     * Performs breadth-first search of a Graph starting in a Vertex
     *
     * @param g Graph instance
     * @param vInf information of the Vertex that will be the source of the
     * search
     * @return qbfs a queue with the vertices of breadth-first search
     */
    public static <V, E> LinkedList<V> BreadthFirstSearch(Graph<V, E> g, V vert) {

        if (!g.validVertex(vert)) {
            return null;
        }

        LinkedList<V> qbfs = new LinkedList<>();
        LinkedList<V> qaux = new LinkedList<>();
        boolean[] visited = new boolean[g.numVertices()];  //default initializ.: false

        qbfs.add(vert);
        qaux.add(vert);
        int vKey = g.getKey(vert);
        visited[vKey] = true;

        while (!qaux.isEmpty()) {
            vert = qaux.remove();
            for (Edge<V, E> edge : g.outgoingEdges(vert)) {
                V vAdj = g.opposite(vert, edge);
                vKey = g.getKey(vAdj);
                if (!visited[vKey]) {
                    qbfs.add(vAdj);
                    qaux.add(vAdj);
                    visited[vKey] = true;
                }
            }
        }
        return qbfs;
    }

    /**
     * Performs depth-first search starting in a Vertex
     *
     * @param g Graph instance
     * @param vOrig Vertex of graph g that will be the source of the search
     * @param visited set of discovered vertices
     * @param qdfs queue with vertices of depth-first search
     */
    private static <V, E> void DepthFirstSearch(Graph<V, E> graph, V vOrig, boolean[] visited, LinkedList<V> qdfs) {
        visited[graph.getKey(vOrig)] = true;
        for (Edge<V, E> edge : graph.outgoingEdges(vOrig)) {
            if (visited[graph.getKey(edge.getVDest())] == false) {
                qdfs.add(edge.getVDest());
                DepthFirstSearch(graph, edge.getVDest(), visited, qdfs);
            }
        }
    }

    /**
     * @param g Graph instance
     * @param vInf information of the Vertex that will be the source of the
     * search
     * @return qdfs a queue with the vertices of depth-first search
     */
    public static <V, E> LinkedList<V> DepthFirstSearch(Graph<V, E> graph, V vertex) {
        if (graph.validVertex(vertex)) {
            boolean[] visitedVertex = new boolean[graph.numVertices()];
            LinkedList<V> qdfs = new LinkedList<>();
            qdfs.add(vertex);
            DepthFirstSearch(graph, vertex, visitedVertex, qdfs);
            return qdfs;
        }
        return null;
    }

    /**
     * Returns all paths from vOrig to vDest
     *
     * @param graph Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param vDest Vertex that will be the end of the path
     * @param visited set of discovered vertices
     * @param path stack with vertices of the current path (the path is in
     * reverse order)
     * @param paths ArrayList with all the paths (in correct order)
     */
    private static <V, E> void allPaths(Graph<V, E> graph, V vOrig, V vDest, boolean[] visited,
            LinkedList<V> path, ArrayList<LinkedList<V>> paths) {
        visited[graph.getKey(vOrig)] = true;
        path.add(vOrig);
        for (Edge<V, E> edge : graph.outgoingEdges(vOrig)) {
            if (edge.getVDest().equals(vDest)) {
                path.add(vDest);
                paths.add(path);
                path.removeLast();
            } else {
                if (visited[graph.getKey(edge.getVDest())] == false) {
                    allPaths(graph, edge.getVDest(), vDest, visited, path, paths);
                }
            }
        }
        visited[graph.getKey(vOrig)] = false;
        path.removeLast();
    }

    /**
     * @param graph Graph instance
     * @param voInf information of the Vertex origin
     * @param vdInf information of the Vertex destination
     * @return paths ArrayList with all paths from voInf to vdInf
     */
    public static <V, E> ArrayList<LinkedList<V>> allPaths(Graph<V, E> graph, V vOrig, V vDest) {
        LinkedList<V> path = new LinkedList<>();
        ArrayList<LinkedList<V>> allPath = new ArrayList<>();
        if (graph.validVertex(vOrig) && graph.validVertex(vDest)) {
            boolean[] visited = new boolean[graph.numVertices()];
            allPaths(graph, vOrig, vDest, visited, path, allPath);
            return allPath;
        }
        return null;
    }

    /**
     * Computes shortest-path distance from a source vertex to all reachable
     * vertices of a graph g with nonnegative edge weights This implementation
     * uses Dijkstra's algorithm
     *
     * @param graph Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param visited set of discovered vertices
     * @param pathkeys minimum path vertices keys
     * @param distanceMin minimum distances
     */
    private static <V, E> void shortestPathLength(Graph<V, E> graph, V vOrig, V[] vertices,
            boolean[] visited, int[] pathKeys, double[] distanceMin) {

        for (int i = 0; i < graph.numVertices(); i++) {
            distanceMin[i] = Double.MAX_VALUE;
            pathKeys[i] = -1;
            visited[i] = false;
        }
        distanceMin[graph.getKey(vOrig)] = 0;
        int index = graph.getKey(vOrig);
        while (index != -1){
            visited[index] = true;
            for (Edge<V, E> outgoingedge : graph.outgoingEdges(vertices[index])){
                int i = graph.getKey(outgoingedge.getVDest());
                if(visited[i] == false && distanceMin[i] > distanceMin[index] +
                        graph.getEdge(vertices[i], vertices[index]).getWeight()){
                    distanceMin[i] = distanceMin[index] + graph.getEdge(vertices[i], vertices[index]).getWeight();
                    pathKeys[i] = index;
                }
            }
            int indexAux = index;
            double distanciaMinima = Double.MAX_VALUE;
            for (int i = 0; i < distanceMin.length; i++) {
                if(visited[i] == false){
                    if(distanceMin[i] < distanciaMinima){
                        distanciaMinima = distanceMin[i];
                        index = i;
                    }
                }
                if (i == distanceMin.length - 1 && index == indexAux){
                    index = -1;
                }
            }
        }
    }

    /**
     * Extracts from pathKeys the minimum path between voInf and vdInf The path
     * is constructed from the end to the beginning
     *
     * @param g Graph instance
     * @param voInf information of the Vertex origin
     * @param vdInf information of the Vertex destination
     * @param pathkeys minimum path vertices keys
     * @param path stack with the minimum path (correct order)
     */
    private static <V, E> void getPath(Graph<V, E> g, V vOrig, V vDest, V[] verts, int[] pathKeys, LinkedList<V> path) {
        path.addFirst(vDest);
        V aux = verts[pathKeys[g.getKey(vDest)]];
        while (g.getKey(aux) != (g.getKey(vOrig))) {
            path.addFirst(aux);
            aux = verts[pathKeys[g.getKey(aux)]];
        }
        path.addFirst(vOrig);
    }

    //shortest-path between voInf and vdInf
    public static <V, E> double shortestPath(Graph<V, E> g, V vOrig, V vDest, LinkedList<V> shortPath) {
        if (g.validVertex(vDest) && g.validVertex(vOrig)) {
            if (vOrig.equals(vDest)) {
                shortPath.add(vDest);
                return 0;
            }
            V[] vertices = g.allkeyVerts();
            boolean[] visited = new boolean[g.numVertices()];
            int[] pathKeys = new int[g.numVertices()];
            double[] dist = new double[g.numVertices()];
            shortestPathLength(g, vOrig, vertices, visited, pathKeys, dist);
            if (pathKeys[g.getKey(vDest)] != -1) {
                getPath(g, vOrig, vDest, vertices, pathKeys, shortPath);
            }

            return dist[g.getKey(vDest)];
        }
        return 0;
    }

    /**
     * Reverses the path
     *
     * @param path stack with path
     */
    private static <V, E> LinkedList<V> revPath(LinkedList<V> path) {

        LinkedList<V> pathcopy = new LinkedList<>(path);
        LinkedList<V> pathrev = new LinkedList<>();

        while (!pathcopy.isEmpty()) {
            pathrev.push(pathcopy.pop());
        }

        return pathrev;
    }
}
