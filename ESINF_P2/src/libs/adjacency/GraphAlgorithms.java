package libs.adjacency;

import java.util.LinkedList;
import java.util.Iterator;

/**
 * Implementation of graph algorithms for a (undirected) graph structure
 * Considering generic vertex V and edge E types
 *
 * Works on AdjancyMatrixGraph objects
 *
 * @author DEI-ESINF
 *
 */
public class GraphAlgorithms {

    private static <T> LinkedList<T> reverse(LinkedList<T> list) {
        LinkedList<T> reversed = new LinkedList<T>();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            reversed.push(it.next());
        }
        return reversed;
    }

    /**
     * Performs depth-first search of the graph starting at vertex. Calls
     * package recursive version of the method.
     *
     * @param graph Graph object
     * @param vertex Vertex of graph that will be the source of the search
     * @return queue of vertices found by search (including vertex), null if
     * vertex does not exist
     *
     */
    public static <V, E> LinkedList<V> DFS(AdjacencyMatrixGraph<V, E> graph, V vertex) {

        int index = graph.toIndex(vertex);
        if (index == -1) {
            return null;
        }

        LinkedList<V> resultQueue = new LinkedList<V>();
        resultQueue.add(vertex);
        boolean[] knownVertices = new boolean[graph.numVertices];
        DFS(graph, index, knownVertices, resultQueue);
        return resultQueue;
    }

    /**
     * Actual depth-first search of the graph starting at vertex. The method
     * adds discovered vertices (including vertex) to the queue of vertices
     *
     * @param graph Graph object
     * @param index Index of vertex of graph that will be the source of the
     * search
     * @param known previously discovered vertices
     * @param verticesQueue queue of vertices found by search
     *
     */
    static <V, E> void DFS(AdjacencyMatrixGraph<V, E> graph, int index,
            boolean[] knownVertices, LinkedList<V> verticesQueue) {
        knownVertices[index] = true;
        for (int i = 0; i < graph.numVertices; i++) {
            if (graph.edgeMatrix[index][i] != null && knownVertices[i] == false) {
                verticesQueue.add(graph.vertices.get(i));
                DFS(graph, i, knownVertices, verticesQueue);
            }
        }
    }

    /**
     * Performs breadth-first search of the graph starting at vertex. The method
     * adds discovered vertices (including vertex) to the queue of vertices
     *
     * @param graph Graph object
     * @param vertex Vertex of graph that will be the source of the search
     * @return queue of vertices found by search (including vertex), null if
     * vertex does not exist
     *
     */
    public static <V, E> LinkedList<V> BFS(AdjacencyMatrixGraph<V, E> graph, V vertex) {
        if (graph.toIndex(vertex) == -1) {
            return null;
        }
        LinkedList<V> queue = new LinkedList<>();
        LinkedList<V> auxqueue = new LinkedList<>();
        boolean[] visited = new boolean[graph.numVertices()];

        queue.add(vertex);
        auxqueue.add(vertex);
        visited[graph.toIndex(vertex)] = true;

        while (!auxqueue.isEmpty()) {
            vertex = auxqueue.remove();
            for (V directConnection : graph.directConnections(vertex)) {
                int vertexIndex = graph.toIndex(directConnection);
                if (!visited[vertexIndex]) {
                    queue.add(directConnection);
                    auxqueue.add(directConnection);
                    visited[vertexIndex] = true;
                }
            }
        }
        return queue;
    }

    /**
     * All paths between two vertices Calls recursive version of the method.
     *
     * @param graph Graph object
     * @param source Source vertex of path
     * @param dest Destination vertex of path
     * @param path LinkedList with paths (queues)
     * @return false if vertices not in the graph
     *
     */
    public static <V, E> boolean allPaths(AdjacencyMatrixGraph<V, E> graph,
            V source, V dest, LinkedList<LinkedList<V>> paths) {
        int indexSource = graph.toIndex(source);
        int indexDest = graph.toIndex(dest);
        if (indexDest != -1 && indexSource != -1) {
            paths.clear();
            boolean[] knownVertices = new boolean[graph.numVertices];
            LinkedList<V> auxStack = new LinkedList<>();
            allPaths(graph, graph.toIndex(source), graph.toIndex(dest),
                    knownVertices, auxStack, paths);
            return true;
        }
        return false;
    }

    /**
     * Actual paths search The method adds vertices to the current path (stack
     * of vertices) when destination is found, the current path is saved to the
     * list of paths
     *
     * @param graph Graph object
     * @param sourceIdx Index of source vertex
     * @param destIdx Index of destination vertex
     * @param knownVertices previously discovered vertices
     * @param auxStack stack of vertices in the path
     * @param path LinkedList with paths (queues)
     *
     */
    static <V, E> void allPaths(AdjacencyMatrixGraph<V, E> graph, int sourceIdx,
            int destIdx, boolean[] knownVertices, LinkedList<V> auxStack,
            LinkedList<LinkedList<V>> paths) {
        
        knownVertices[sourceIdx] = true;
        auxStack.add(graph.vertices.get(sourceIdx));
        
        for (V directConnection : graph.directConnections(graph.vertices.get(
                sourceIdx))) {
            
            if (directConnection.equals(graph.vertices.get(destIdx))) {
                
                auxStack.add(graph.vertices.get(destIdx));
                paths.add(auxStack);
                auxStack.removeLast();
                
            } else if (!knownVertices[graph.toIndex(directConnection)]) {
                allPaths(graph, graph.toIndex(directConnection), destIdx, 
                        knownVertices, auxStack,paths);
            }
        }
        knownVertices[sourceIdx] = false;
        auxStack.removeLast();
    }

    /**
     * Transforms a graph into its transitive closure uses the Floyd-Warshall
     * algorithm
     *
     * @param graph Graph object
     * @param dummyEdge object to insert in the newly created edges
     * @return the new graph
     */
    public static <V, E> AdjacencyMatrixGraph<V, E> transitiveClosure(
            AdjacencyMatrixGraph<V, E> graph, E dummyEdge) {
        AdjacencyMatrixGraph<V, E> aux = new AdjacencyMatrixGraph<>();
        for (int k = 0; k < graph.numVertices; k++) {
            for (int i = 0; i < graph.numVertices; i++) {

                if (i != k && graph.edgeMatrix[i][k] != null) {

                    for (int j = 0; j < graph.numVertices; j++) {
                        if (i != j && k != j && graph.edgeMatrix[k][j] != null) {
                            aux.edgeMatrix[i][j] = dummyEdge;
                        }
                    }
                }
            }
        }
        return aux;
    }

}
