/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import java.util.Objects;

/**
 *
 * @author Filipe Ferreira
 * @author Vitor Melo
 */
public class Alianca {

    private float compatibilidade;
    private Personagem per1;
    private Personagem per2;
    private boolean publica;
    
    public Alianca(boolean publica, float compatibilidade, Personagem per1, Personagem per2) {
        this.publica = publica;
        this.compatibilidade = validarFatorComp(compatibilidade) ? compatibilidade : -1;
        this.per1 = per1;
        this.per2 = per2;
    }

    public Personagem getPer1() {
        return per1;
    }

    public float getCompatibilidade() {
        return compatibilidade;
    }

    public Personagem getPer2() {
        return per2;
    }

    public boolean isPublica() {
        return publica;
    }

    public boolean validarFatorComp(float f) {
        return (f >= 0 && f <= 1);
    }
    
    public float calculateForca() {
        int forca1 = per1.getForca();
        int forca2 = per2.getForca();

        return (forca1 + forca2) * compatibilidade;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Alianca pObj = (Alianca) obj;
        return pObj.per1.equals(per1) && pObj.per2.equals(per2) && pObj.publica == publica;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.per1);
        hash = 37 * hash + Objects.hashCode(this.per2);
        hash = 37 * hash + (this.publica ? 1 : 0);
        return hash;
    }

}
