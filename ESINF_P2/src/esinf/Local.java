/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import java.util.Objects;

/**
 *
 * @author Filipe Ferreira
 * @author Vitor Melo
 */
public class Local {
    
    /**
     * Atributo nome do local
     */
    private String nome;
    
    //Map para as personagens
    
    /**
     * Atributo pontos do local, relacionado com a dificuldade de conquista
     */
    private int pontos;
    
    /**
     * 
     */
    private Personagem dono;
    
    /**
     * Construtor
     * 
     * @param nome
     * @param pontos 
     */
    public Local(String  nome, int pontos){
        this.nome = nome;
        this.pontos = pontos;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return the pontos
     */
    public int getPontos() {
        return pontos;
    }
    
    /**
     * @return the dono
     */
    public Personagem getDono() {
        return dono;
    }
    
    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @param pontos the pontos to set
     */
    public void setPontos(int pontos) {
        this.pontos = pontos;
    }
    
    /**
     * @param dono the dono to set
     */
    public void setDono(Personagem dono) {
        this.dono = dono;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Local lObj = (Local) obj;
        return lObj.nome.equalsIgnoreCase(nome);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.nome);
        return hash;
    }
}
