/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import java.util.Objects;

/**
 *
 * @author Filipe Ferreira
 * @author Vitor Melo
 */
public class Personagem {
    
    /**
     * Atributo nome da personagem
     */
    private String nome;

    /**
     * Atributo forca da personagem, pontos de força
     */
    private int forca;

    /**
     * Construtor
     *
     * @param nome
     * @param forca
     */
    public Personagem(String nome, int forca) {
        this.nome = nome;
        this.forca = forca;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return the forca
     */
    public int getForca() {
        return forca;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @param forca the forca to set
     */
    public void setForca(int forca) {
        this.forca = forca;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Personagem pObj = (Personagem) obj;
        return pObj.nome.equalsIgnoreCase(nome);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.nome);
        return hash;
    }
}
