/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

/**
 *
 * @author Filipe Ferreira
 * @author Vitor Melo
 */
public class Estrada {

    /**
     * Atributo pontos do caminho, a dificuldade de o percorrer
     */
    private int pontos;

    /**
     * Construtor
     *
     * @param pontos
     */
    public Estrada(int pontos) {
        this.pontos = pontos;
    }

    /**
     * @return the pontos
     */
    public int getPontos() {
        return pontos;
    }

    /**
     * @param pontos the pontos to set
     */
    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

}
