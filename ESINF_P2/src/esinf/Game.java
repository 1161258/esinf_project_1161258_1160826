/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import libs.adjacency.AdjacencyMatrixGraph;
import libs.mapgraph.Edge;
import libs.mapgraph.Graph;
import libs.mapgraph.GraphAlgorithms;

/**
 *
 * @author Filipe Ferreira
 * @author Vitor Melo
 */
public class Game {

    private AdjacencyMatrixGraph<Local, Estrada> mapaTerreno;
    private Graph<Personagem, Alianca> aliancaPersonagens;

    public Game() {
        this.mapaTerreno = new AdjacencyMatrixGraph<>();
        this.aliancaPersonagens = new Graph<>(false);
    }

    public AdjacencyMatrixGraph<Local, Estrada> getMapaTerreno() {
        return mapaTerreno;
    }

    public Graph<Personagem, Alianca> getAliancaPersonagens() {
        return aliancaPersonagens;
    }

    public void setAliancaPersonagens(Graph<Personagem, Alianca> aliancaPersonagens) {
        this.aliancaPersonagens = aliancaPersonagens;
    }

    public void setMapaTerreno(AdjacencyMatrixGraph<Local, Estrada> mapaTerreno) {
        this.mapaTerreno = mapaTerreno;
    }

    /**
     * alinea 1-A
     *
     * @param l
     * @param p
     */
    public void readLocalEstrada(List<String> l, List<String> p) {
        Iterator<String> it = l.iterator();
        if (it.next().equalsIgnoreCase("LOCAIS")) {
            readLocais(l, p);
        }
    }

    /**
     * Alinea 1A
     *
     * @param fichL
     */
    private void readLocais(List<String> fichL, List<String> fichP) {
        LinkedList<Local> locais = new LinkedList<>();
        int stoppedIndex = -1;
        for (int i = 1; i < fichL.size(); i++) {
            String s = fichL.get(i);
            if (!s.isEmpty()) {
                if (!s.equals("CAMINHOS")) {
                    String[] linhaSplit = s.split(",");
                    locais.add(new Local(linhaSplit[0], Integer.parseInt(
                            linhaSplit[1])));
                } else {
                    stoppedIndex = i;
                    break;
                }
            }
        }
        for (int i = 1; i < fichP.size(); i++) {
            String s = fichP.get(i);
            if (!s.isEmpty()) {
                if (!s.equals("ALIANÇAS")) {
                    String[] linhaSplit = s.split(",");
                    Personagem p = new Personagem(linhaSplit[0], Integer.
                            parseInt(linhaSplit[1]));
                    for (Local local : locais) {
                        if (local.getNome().equals(linhaSplit[2])) {
                            local.setDono(p);
                            break;
                        }
                    }
                } else {
                    break;
                }
            }
        }
        for (Local local : locais) {
            mapaTerreno.insertVertex(local);
        }
        readCaminhos(fichP, stoppedIndex);
    }

    /**
     * Alinea 1A
     *
     * @param fich
     * @param i
     */
    private void readCaminhos(List<String> fich, int i) {
        for (int j = i + 1; j < fich.size(); j++) {
            if (!fich.get(j).isEmpty()) {
                String[] linhaSplit = fich.get(j).split(",");
                Estrada e = new Estrada(Integer.parseInt(linhaSplit[2]));

                Iterable<Local> vertexList = mapaTerreno.vertices();
                ArrayList<Local> auxVertexSave = new ArrayList<>();

                for (Local local : vertexList) {
                    if (local.getNome().equals(linhaSplit[0])
                            || local.getNome().equals(linhaSplit[1])) {
                        auxVertexSave.add(local);
                    }
                }
                mapaTerreno.
                        insertEdge(auxVertexSave.get(0), auxVertexSave.get(1), e);
            }
        }
    }

    /**
     * Alinea 1B
     *
     * Djikstra, obter o caminho mais curto
     * 
     * @param orig
     * @param dest
     * @return
     */
    public LinkedList<Local> lowerDifficultyPath(Local orig, Local dest) {

        if (mapaTerreno.checkVertex(orig) && mapaTerreno.checkVertex(dest)) {
            boolean[] visited = new boolean[mapaTerreno.numVertices()];
            HashMap<LinkedList<Local>, Double> edgeValue = new HashMap<>();

            AdjacencyMatrixGraph<Local, Estrada> copy
                    = (AdjacencyMatrixGraph<Local, Estrada>) mapaTerreno.clone();

            HashMap<LinkedList<Local>, Double> paths
                    = lowerDifficultyPath(copy, orig, dest, visited, edgeValue);

            return getSmallerPathWithDestiny(paths, dest);

        }
        return null;
    }

    /**
     * Alinea 1B
     *
     * Djikstra algoritmo, retorna mapa com os caminhos e respetivas distancias
     * 
     * @param graph
     * @param orig
     * @param dest
     * @param visited
     * @param edgeValue
     * @return
     */
    private HashMap<LinkedList<Local>, Double> lowerDifficultyPath(
            AdjacencyMatrixGraph<Local, Estrada> graph,
            Local orig, Local dest, boolean[] visited,
            HashMap<LinkedList<Local>, Double> edgeValue) {

        //fator de terminaçao
        if (orig.equals(dest)) {
            return edgeValue;
        }

        //coloca marca como true que ja foi visitado
        visited[graph.toIndex(orig)] = true;
        //obtem as ligacoes diretas a origem
        Iterable<Local> directCon = graph.directConnections(orig);

        //se for a primeira vez a executar a lista estara vazia portanto para
        //cada ligacao direta preenche o map com a dificuldade da estrada
        if (edgeValue.isEmpty()) {
            for (Local local : directCon) {
                LinkedList<Local> locs = new LinkedList<>();
                locs.add(orig);
                locs.add(local);
                double value = graph.getEdge(orig, local).getPontos();
                edgeValue.put(locs, value);
            }
        } else {
            //se o map estiver com valores 
            //obtem as chaves
            Set<LinkedList<Local>> tempSet = edgeValue.
                    keySet();
            //cria variaveis temporarias para guardar o caminho mais curto
            //busca a chave que contem a origem
            LinkedList<Local> tempPath = getSmallerPathWithOrig(edgeValue, orig);
            double tempValue = edgeValue.get(tempPath);

            //remove a chave e o valor do map
            edgeValue.remove(tempPath);
            //se o local ja tiver sido visitado salta para a iteracao seguinte
            //para cada ligaçao direta clona o caminho temporario, 
            //adiciona o caminho direto e soma o valor do caminho ao obtido
            //guardando-o de novo no map
            for (Local local : directCon) {
                if (!visited[graph.toIndex(local)]) {
                    LinkedList<Local> newPath = (LinkedList<Local>) tempPath.
                            clone();
                    newPath.add(local);
                    double newValue = tempValue + graph.getEdge(tempPath.
                            getLast(),
                            local).getPontos();
                    edgeValue.put(newPath, newValue);
                }
            }
        }

        //por fim obtem as chaves e verifica o menor caminho existente
        //chama a funcao
        return lowerDifficultyPath(graph, getSmallerPath(edgeValue).getLast(),
                dest, visited, edgeValue);

    }

    /**
     * Alinea 1B
     * 
     * Obtem o menor caminho com o local de origem
     * 
     * @param edgeValue
     * @param orig
     * @return
     */
    private LinkedList<Local> getSmallerPathWithOrig(
            HashMap<LinkedList<Local>, Double> edgeValue, Local orig) {

        LinkedList<Local> listToReturn = null;
        if (!edgeValue.isEmpty()) {
            double smallerValue = Double.MAX_VALUE;
            for (LinkedList<Local> linkedList : edgeValue.keySet()) {
                if (linkedList.contains(orig)) {
                    double auxValue = edgeValue.get(linkedList);
                    if (smallerValue > auxValue) {
                        smallerValue = auxValue;
                        listToReturn = linkedList;
                    }
                }
            }
        }
        return listToReturn;
    }

    /**
     * Alinea 1B
     *
     * Obtem o menor caminho
     * 
     * @param edgeValue
     * @return
     */
    private LinkedList<Local> getSmallerPath(
            HashMap<LinkedList<Local>, Double> edgeValue) {
        Set<LinkedList<Local>> set = edgeValue.keySet();
        LinkedList<Local> smaller = new LinkedList<>();
        double smallerValue = Double.MAX_VALUE;
        for (LinkedList<Local> linkedList : set) {
            double auxSmaller = edgeValue.get(linkedList);
            if (auxSmaller < smallerValue) {
                smallerValue = auxSmaller;
                smaller = linkedList;
            }
        }
        return smaller;
    }

    /**
     * Alinea 1B
     *
     * Obtem o menor caminho com o destino
     * 
     * @param edgeValue
     * @param dest
     * @return
     */
    private LinkedList<Local> getSmallerPathWithDestiny(
            HashMap<LinkedList<Local>, Double> edgeValue, Local dest) {
        LinkedList<Local> listToReturn = null;
        if (!edgeValue.isEmpty()) {
            double value = Double.MAX_VALUE;
            for (LinkedList<Local> linkedList : edgeValue.keySet()) {
                if (linkedList.contains(dest) && edgeValue.get(linkedList) < value) {
                    listToReturn = linkedList;
                    value = edgeValue.get(linkedList);
                }
            }
        }
        return listToReturn;
    }

    /**
     * Alinea 1C
     *
     * Funciona como o uma mascara para a pathToConquer
     *
     * @param dest
     * @param pInicial
     * @param path
     * @return
     */
    public boolean canPerConquer(Local dest, Personagem pInicial,
            Map<LinkedList<Local>, Double> path) {
        pathToConquer(dest, pInicial, path);
        double val = -1;
        if (path.size() == 1) {
            for (LinkedList<Local> linkedList : path.keySet()) {
                val = path.get(linkedList);
            }
        }
        return pInicial.getForca() > val;
    }

    /**
     * Alinea 1C e 3F
     *
     * @param dest
     * @param pInicial
     * @param path
     * @return
     */
    private void pathToConquer(Local dest, Personagem pInicial,
            Map<LinkedList<Local>, Double> path) {

        AdjacencyMatrixGraph<Local, Estrada> graphCopy
                = changeGraphWeights(mapaTerreno, pInicial);

        //verifica as origens da personagem
        Iterable<Local> locais = graphCopy.vertices();
        ArrayList<Local> origens = new ArrayList<>();
        for (Local local : locais) {
            if (local.getDono() != null) {
                if (local.getDono().equals(pInicial)) {
                    origens.add(local);
                }
            }
        }

        //se nao tiver origens limpa os caminhos e para o algoritmo
        if (origens.isEmpty()) {
            path.clear();
            return;
        }

        //obtem os caminhos e valores para cada local de origem
        ArrayList<HashMap<LinkedList<Local>, Double>> caminhos = new ArrayList<>();
        for (int i = 0; i < origens.size(); i++) {
            caminhos.add(getAllPaths(mapaTerreno, origens.get(i), dest));
        }

        //calcula o valor total para cada caminho
        for (HashMap<LinkedList<Local>, Double> caminho : caminhos) {
            for (LinkedList<Local> linkedList : caminho.keySet()) {
                caminho.replace(linkedList, calcularCustoTotalGrafoTransformado(
                        graphCopy, linkedList));
            }
        }
        //obtem o caminho com menos distancia
        LinkedList<Local> melhorCaminho = new LinkedList<>();
        double valor = Double.MAX_VALUE;
        for (HashMap<LinkedList<Local>, Double> caminho : caminhos) {
            for (LinkedList<Local> linkedList : caminho.keySet()) {
                if (caminho.get(linkedList) < valor) {
                    valor = caminho.get(linkedList);
                    melhorCaminho = linkedList;
                }
            }
        }
        //guarda o caminho no map com o valor associado
        path.put(copyPath(melhorCaminho), valor);

    }

    /**
     * Alinea 1C
     *
     * Cria uma cópia da linkedlist
     *
     * @param bestPath
     * @param path
     */
    private LinkedList<Local> copyPath(LinkedList<Local> path) {
        LinkedList<Local> newList = new LinkedList<>();
        for (Local local : path) {
            newList.add(local);
        }
        return newList;
    }

    /**
     * Alinea 1C
     *
     * Calcula o peso total de um dado caminho recorrendo ao grafo transformado
     *
     * @param graph
     * @param vertices
     * @return
     */
    private double calcularCustoTotalGrafoTransformado(
            AdjacencyMatrixGraph<Local, Estrada> graph, LinkedList<Local> vertices) {
        double custo = 0;
        for (int i = 0; i < vertices.size() - 1; i++) {
            custo += graph.getEdge(vertices.get(i), vertices.get(i + 1)).
                    getPontos() - vertices.get(i).getPontos();
        }
        return custo;
    }

    /**
     * Alinea 1C
     *
     * Altera o peso do grafo ao criar uma cópia, soma o peso dos vertices
     * juntamente com a força da personagem do vertice desde que nao seja o
     * indicado
     *
     * Os valores dos vertices ficam inalterados bem como o valor das
     * personagens para uso no cálculo do melhor caminho
     *
     * @param graph
     * @param per
     * @return
     */
    private AdjacencyMatrixGraph<Local, Estrada> changeGraphWeights(
            AdjacencyMatrixGraph<Local, Estrada> graph, Personagem per) {

        AdjacencyMatrixGraph<Local, Estrada> grapMod
                = (AdjacencyMatrixGraph<Local, Estrada>) graph.clone();

        for (Estrada edge : grapMod.edges()) {
            Object[] locais = grapMod.endVertices(edge);
            Local a = (Local) locais[0];
            Local b = (Local) locais[1];
            int aPow = a.getPontos();
            int bPow = b.getPontos();
            if (a.getDono() != null && !a.getDono().equals(per)) {
                aPow += a.getDono().getForca();
            }
            if (b.getDono() != null && !b.getDono().equals(per)) {
                bPow += b.getDono().getForca();
            }
            edge.setPontos(edge.getPontos() + aPow + bPow);
            grapMod.insertEdge(a, b, edge);
        }
        return grapMod;
    }

    /**
     * Alinea 1C
     *
     * Usa o djikstra para obter todos os caminhos e seus pesos, retorna null se
     * um dos vertices nao existir
     *
     * @param graph
     * @param orig
     * @param dest
     * @return
     */
    private HashMap<LinkedList<Local>, Double> getAllPaths(
            AdjacencyMatrixGraph<Local, Estrada> graph, Local orig, Local dest) {

        if (graph.checkVertex(orig) && graph.checkVertex(dest)) {
            boolean[] visited = new boolean[graph.numVertices()];
            HashMap<LinkedList<Local>, Double> edgeValue = new HashMap<>();
            AdjacencyMatrixGraph<Local, Estrada> copy
                    = (AdjacencyMatrixGraph<Local, Estrada>) graph.clone();

            HashMap<LinkedList<Local>, Double> paths
                    = lowerDifficultyPath(copy, orig, dest, visited, edgeValue);

            return paths;

        }
        return null;
    }

    /**
     * Alínea 2-A
     * Método que irá ler todas as personagens e efetuar as alianças das mesmas
     * @param list 
     */
    public void readPersonagenAlianca(List<String> list) {
        Iterator<String> iterator = list.iterator();
        if (iterator.next().equalsIgnoreCase("PERSONAGENS")) {
            readPersonagem(list);
        }
    }

    /**
     * Alínea 2-A_Auxiliar
     * Trata as linhas referentes às personagens
     * @param ficheiro ficheiro onde se encontra as personagens e alianças
     */
    private void readPersonagem(List<String> ficheiro) {
        for (int i = 1; i < ficheiro.size(); i++) {
            String s = ficheiro.get(i);
            if (!s.isEmpty()) {
                if (!s.equalsIgnoreCase("ALIANÇAS")) {
                    String[] split = s.split(",");
                    aliancaPersonagens.insertVertex(new Personagem(split[0],
                            Integer.parseInt(split[1])));

                } else {
                    readAlianca(ficheiro, i);
                }
            }
        }
    }

    /**
     * Alínea 2-A_Axiliar
     * Trata as linhas referentes às alianças 
     * @param ficheiro ficheiro onde se encontra as personagens e alianças 
     * @param i index em que ficou no método anterior
     */
    private void readAlianca(List<String> ficheiro, int i) {
        for (int j = i + 1; j < ficheiro.size(); j++) {
            String[] split = ficheiro.get(j).split(",");

            Iterable<Personagem> listaVertices = aliancaPersonagens.vertices();
            ArrayList<Personagem> aux = new ArrayList<>();
            for (Personagem per : listaVertices) {
                if (per.getNome().equals(split[0]) || per.getNome().equals(
                        split[1])) {
                    aux.add(per);
                }
            }
            Alianca a = new Alianca(Boolean.parseBoolean(split[2]), Float.
                    parseFloat(split[3]),
                    aux.get(0), aux.get(1));
            aliancaPersonagens.insertEdge(a.getPer1(), a.getPer2(), a, i);
        }
    }

    /**
     * Alínea 2-B
     * Método que verifica quais os aliados de uma personagem
     * @param p uma determinada personagem para a qual queremos ver os aliados
     * @return da lista dos aliados da personagem
     */
    public List<Personagem> aliadosPersonagem(Personagem p) {
        List<Personagem> allies = new ArrayList<>();
        Iterable<Personagem> perso = aliancaPersonagens.vertices();
        for (Personagem per : perso) {
            //Se existir um caminho direto entre as personagens, então é aliado 
            if (!(p.equals(per)) && aliancaPersonagens.getEdge(p, per) != null) {
                allies.add(per);
            }
        }
        return allies;
    }

    /**
     * Alínea 2-C
     * Método para determinar a aliança mais forte
     * @return  map com a aliança (força, personagens)
     */
    public Map<Float, Personagem[]> getAliancaMaisForte() {
        Map<Float, Personagem[]> aMaisForte = new HashMap<>();
        float forcaAlianca = 0;
        Edge<Personagem, Alianca> tempEdge = null;
        for (Edge<Personagem, Alianca> edge : aliancaPersonagens.getAllEdges()) {
            if (edge.getElement() != null) {
                float aux = edge.getElement().calculateForca();
                if (aux > forcaAlianca) {
                    forcaAlianca = aux;
                    tempEdge = edge;
                }
            }
        }
        aMaisForte.put(forcaAlianca, copyEndVertices(aliancaPersonagens.
                endVertices(tempEdge)));
        return aMaisForte;
    }

    /**
     * Alinea 2C
     *
     * Faz uma copia exata, retorna null para medida de protecao
     *
     * @param objects
     * @return
     */
    private Personagem[] copyEndVertices(Object[] objects) {
        if (objects != null && objects.length == 2) {
            Personagem[] pers = new Personagem[2];
            for (int i = 0; i < pers.length; i++) {
                pers[i] = (Personagem) objects[i];
            }
            return pers;
        }
        return null;
    }

    /**
     * Alínea 2-D
     * Método para criar uma nova personagem
     * @param perA 
     * @param perB
     * @return de um boleano afirmar se criou a aliança
     */
    public boolean newAlianca(Personagem perA, Personagem perB) {
        //verifica se os vertices são válidos
        if (aliancaPersonagens.validVertex(perA) && aliancaPersonagens.
                validVertex(perB)) {
            //lista dos vertices que visitará
            LinkedList<Personagem> listVisitados = new LinkedList<>();
            //calcula a distancia do menor caminho entre 2 personagensm, para o 
            //caso de estas pertencerem à mesma rede de alianças
            float distance = (float) GraphAlgorithms.shortestPath(
                    aliancaPersonagens, perA, perB, listVisitados);
            //Verifica se já são aliados, se sim, cria uma nova aliança á mesma
            List<Personagem> aliadosA = aliadosPersonagem(perA);
            for (Personagem p : aliadosA) {
                if (perB.equals(p)) {
                    aliancaPersonagens.removeEdge(perA, perB);
                    newAliancaMesmaRede(perA, perB, distance, listVisitados);
                    return true;
                }
            }
            //quando a distância entre A e B é diferente de 0 ele criará aliança
            if (distance != 0) {
                newAliancaMesmaRede(perA, perB, distance, listVisitados);
                return true;
            }
        }
        return false;
    }

    /**
     * Alínea 2-D Auxiliar
     */
    private void newAliancaMesmaRede(Personagem perA, Personagem perB, float distance,
            LinkedList<Personagem> listVisitados) {
        Alianca novaA = new Alianca(true, distance / (listVisitados.size() - 1),
                perA, perB);
        aliancaPersonagens.insertEdge(perA, perB, novaA, 0);
    }

    /**
     * Alinea 2E
     *
     * @return
     */
    public Graph<Personagem, Alianca> getPossibleAlliances() {
        Iterable<Personagem> vertices = aliancaPersonagens.vertices();
        //se nao houver vertices retorna null
        if (vertices == null) {
            return null;
        }
        //para cada vertice guarda no novo grafo e numa lista
        List<Personagem> allVertices = new ArrayList<>();
        Graph<Personagem, Alianca> possibleAlliances = new Graph<>(false);
        for (Personagem vertice : vertices) {
            possibleAlliances.insertVertex(vertice);
            allVertices.add(vertice);
        }
        //para cada personagem verifica os aliados e nao aliados
        for (Personagem vertice : vertices) {
            List<Personagem> pers = aliadosPersonagem(vertice);
            List<Personagem> notLinked = findDifferentPersona(pers, allVertices);
            //faz ligacao com os que nao sao aliados
            for (Personagem personagem : notLinked) {
                possibleAlliances.insertEdge(vertice, personagem, new Alianca(
                        true, 0.8f, vertice, personagem), 0.8f);
            }
        }
        return possibleAlliances;
    }

    /**
     * Alinea 2E
     *
     * @param directConn
     * @param allPers
     * @return
     */
    private List<Personagem> findDifferentPersona(List<Personagem> directConn,
            List<Personagem> allPers) {
        List<Personagem> notExistentVert = new ArrayList<>();
        for (Personagem personagem : allPers) {
            if (!existsInList(directConn, personagem)) {
                notExistentVert.add(personagem);
            }
        }
        return notExistentVert;
    }

    /**
     * Alinea 2E
     *
     * @param list
     * @param per
     * @return
     */
    private boolean existsInList(List<Personagem> list, Personagem per) {
        for (Personagem personagem : list) {
            if (per.equals(personagem)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Alínea 3-F
     *
     * @param per
     * @param dest
     * @param caminho
     * @return
     */
    public Personagem canAlliesConquer(Personagem per, Local dest,
            Map<LinkedList<Local>, Double> caminho) {

        if (!aliancaPersonagens.validVertex(per)) {
            return null;
        } else if (aliadosPersonagem(per).isEmpty()) {
            return null;
        } else if (!mapaTerreno.checkVertex(dest)) {
            return null;
        }
        //obtem o melhor caminho para a conquista
        pathToConquer(dest, per, caminho);

        if (caminho.isEmpty()) {
            return null;
        }
        //cria uma lista de aliados potenciais
        LinkedList<Personagem> potentialAllies = new LinkedList<>();
        //verifica quais os aliados que pertencem ao caminho e quais os que nao
        //pertencem, colocando-os na lista
        for (Personagem personagem : aliadosPersonagem(per)) {
            for (LinkedList<Local> linkedList : caminho.keySet()) {
                if (!foundInPath(linkedList, personagem)) {
                    potentialAllies.add(personagem);
                }
            }
        }
        //verifica o primeiro do melhor aliado que contem a força para a conquista
        Personagem bestAlly = null;
        for (LinkedList<Local> linkedList : caminho.keySet()) {
            for (Personagem ally : potentialAllies) {
                if (caminho.get(linkedList) < aliancaPersonagens.getEdge(per,
                        ally).getElement().calculateForca()) {
                    bestAlly = ally;
                    break;
                }
            }
        }
        return bestAlly;

    }

    /**
     * Alinea 3F
     *
     * @param path
     * @param per
     * @return
     */
    private boolean foundInPath(LinkedList<Local> path, Personagem per) {
        for (Local local : path) {
            if (local.getDono() != null && local.getDono().equals(per)) {
                return true;
            }
        }
        return false;
    }

}
