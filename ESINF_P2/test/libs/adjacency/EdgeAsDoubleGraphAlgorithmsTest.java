package libs.adjacency;

import java.util.Iterator;
import java.util.LinkedList;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author DEI-ESINF
 */
public class EdgeAsDoubleGraphAlgorithmsTest {

    AdjacencyMatrixGraph<String, Double> distanceMap = new AdjacencyMatrixGraph<>();

    public EdgeAsDoubleGraphAlgorithmsTest() {
    }

    @Before
    public void setUp() throws Exception {
        distanceMap.insertVertex("Porto");
        distanceMap.insertVertex("Braga");
        distanceMap.insertVertex("Vila Real");
        distanceMap.insertVertex("Aveiro");
        distanceMap.insertVertex("Coimbra");
        distanceMap.insertVertex("Leiria");

        distanceMap.insertVertex("Viseu");
        distanceMap.insertVertex("Guarda");
        distanceMap.insertVertex("Castelo Branco");
        distanceMap.insertVertex("Lisboa");
        distanceMap.insertVertex("Faro");
        distanceMap.insertVertex("Évora");

        distanceMap.insertEdge("Porto", "Aveiro", 75.0);
        distanceMap.insertEdge("Porto", "Braga", 60.0);
        distanceMap.insertEdge("Porto", "Vila Real", 100.0);
        distanceMap.insertEdge("Viseu", "Guarda", 75.0);
        distanceMap.insertEdge("Guarda", "Castelo Branco", 100.0);
        distanceMap.insertEdge("Aveiro", "Coimbra", 60.0);
        distanceMap.insertEdge("Coimbra", "Lisboa", 200.0);
        distanceMap.insertEdge("Coimbra", "Leiria", 80.0);
        distanceMap.insertEdge("Aveiro", "Leiria", 120.0);
        distanceMap.insertEdge("Leiria", "Lisboa", 150.0);

        distanceMap.insertEdge("Aveiro", "Viseu", 85.0);
        distanceMap.insertEdge("Leiria", "Castelo Branco", 170.0);
        distanceMap.insertEdge("Lisboa", "Faro", 280.0);

    }

    @Test
    public void testShortestPath() {
        System.out.println("Test of shortest path");

        LinkedList<String> path = new LinkedList<String>();

        assertTrue("Should be -1 if vertex does not exist",
                EdgeAsDoubleGraphAlgorithms.shortestPath(distanceMap, "Porto",
                        "LX", path) == -1);

        assertTrue("Should be -1 if there is no path",
                EdgeAsDoubleGraphAlgorithms.shortestPath(distanceMap, "Porto",
                        "Évora", path) == -1);

        assertTrue("Should be 0 if source and vertex are the same",
                EdgeAsDoubleGraphAlgorithms.shortestPath(distanceMap, "Porto",
                        "Porto", path) == 0);

        assertTrue(
                "Path should be single vertex if source and vertex are the same",
                path.size() == 1);

        assertTrue("Path between Porto and Lisboa should be 335 Km",
                EdgeAsDoubleGraphAlgorithms.shortestPath(distanceMap, "Porto",
                        "Lisboa", path) == 335);

        Iterator<String> it = path.iterator();

        assertTrue("First in path should be Porto",
                it.next().compareTo("Porto") == 0);
        assertTrue("then Aveiro", it.next().compareTo("Aveiro") == 0);
        assertTrue("then Coimbra", it.next().compareTo("Coimbra") == 0);
        assertTrue("then Lisboa", it.next().compareTo("Lisboa") == 0);

        assertTrue("Path between Braga and Leiria should be 255 Km",
                EdgeAsDoubleGraphAlgorithms.shortestPath(distanceMap, "Braga",
                        "Leiria", path) == 255);

        it = path.iterator();

        assertTrue("First in path should be Braga",
                it.next().compareTo("Braga") == 0);
        assertTrue("then Porto", it.next().compareTo("Porto") == 0);
        assertTrue("then Aveiro", it.next().compareTo("Aveiro") == 0);
        assertTrue("then Leiria", it.next().compareTo("Leiria") == 0);

        assertTrue("Path between Porto and Castelo Branco should be 335 Km",
                EdgeAsDoubleGraphAlgorithms.shortestPath(distanceMap, "Porto",
                        "Castelo Branco", path) == 335);
        assertTrue("Path between Porto and Castelo Branco should be 5 cities",
                path.size() == 5);

        it = path.iterator();

        assertTrue("First in path should be Porto",
                it.next().compareTo("Porto") == 0);
        assertTrue("then Aveiro", it.next().compareTo("Aveiro") == 0);
        assertTrue("then Viseu", it.next().compareTo("Viseu") == 0);
        assertTrue("then Guarda", it.next().compareTo("Guarda") == 0);
        assertTrue("then Castelo Branco",
                it.next().compareTo("Castelo Branco") == 0);

        // Changing Viseu to Guarda should change shortest path between Porto and Castelo Branco
        distanceMap.removeEdge("Viseu", "Guarda");
        distanceMap.insertEdge("Viseu", "Guarda", 125.0);

        assertTrue("Path between Porto and Castelo Branco should now be 365 Km",
                EdgeAsDoubleGraphAlgorithms.shortestPath(distanceMap, "Porto",
                        "Castelo Branco", path) == 365);
        assertTrue("Path between Porto and Castelo Branco should be 4 cities",
                path.size() == 4);

        it = path.iterator();

        assertTrue("First in path should be Porto",
                it.next().compareTo("Porto") == 0);
        assertTrue("then Aveiro", it.next().compareTo("Aveiro") == 0);
        assertTrue("then Leiria", it.next().compareTo("Leiria") == 0);
        assertTrue("then Castelo Branco",
                it.next().compareTo("Castelo Branco") == 0);

    }

    @Test
    public void testMinDistGraph() {
        System.out.println("Test of minimum distances graph");

        AdjacencyMatrixGraph<String, Double> newGraph = EdgeAsDoubleGraphAlgorithms.
                minDistGraph(distanceMap);

        Iterator<String> itVertices = newGraph.vertices().iterator();

        assertTrue("First vertex in graph should be Porto", itVertices.next().
                compareTo("Porto") == 0);
        assertTrue("then Braga", itVertices.next().compareTo("Braga") == 0);
        assertTrue("then Vila Real",
                itVertices.next().compareTo("Vila Real") == 0);
        assertTrue("then Aveiro", itVertices.next().compareTo("Aveiro") == 0);
        assertTrue("then Coimbra", itVertices.next().compareTo("Coimbra") == 0);
        assertTrue("then Leiria", itVertices.next().compareTo("Leiria") == 0);
        assertTrue("then Viseu", itVertices.next().compareTo("Viseu") == 0);
        assertTrue("then Guarda", itVertices.next().compareTo("Guarda") == 0);
        assertTrue("then Castelo Branco", itVertices.next().compareTo(
                "Castelo Branco") == 0);
        assertTrue("then Lisboa", itVertices.next().compareTo("Lisboa") == 0);
        assertTrue("then Faro", itVertices.next().compareTo("Faro") == 0);
        assertTrue("then Évora", itVertices.next().compareTo("Évora") == 0);

        Iterator<Double> itEdges = newGraph.edges().iterator();

        //Porto - Porto
        assertTrue("First edge should be 60 (Porto - Braga)",
                itEdges.next() == 60);
        assertTrue("then 100 (Porto - Vila Real)", itEdges.next() == 100);
        assertTrue("then 75 (Porto - Aveiro)", itEdges.next() == 75);
        assertTrue(
                "then 75 (Porto - Aveiro) + 60 (Aveiro - Coimbra) = 135 (Porto - Coimbra)",
                itEdges.next() == 135);
        assertTrue(
                "then 75 (Porto - Aveiro) + 120 (Aveiro - Leiria) = 195 (Porto - Leiria)",
                itEdges.next() == 195);
        assertTrue(
                "then 75 (Porto - Aveiro) + 85 (Aveiro - Viseu) = 160 (Porto - Viseu)",
                itEdges.next() == 160);
        assertTrue(
                "then 160 (Porto - Viseu) + 75 (Viseu - Guarda) = 235 (Porto - Guarda)",
                itEdges.next() == 235);
        assertTrue(
                "then 235 (Porto - Guarda) + 100 (Guarda - Castelo Branco) = 335 (Porto - Castelo Branco)",
                itEdges.next() == 335);

        itEdges.next(); //Porto - Lisboa
        itEdges.next(); //Porto - Faro
        //Porto - Évora

        assertTrue(
                "then 60 (Porto - Braga) + 100 (Porto - Vila Real) = 160 (Braga - Vila Real)",
                itEdges.next() == 160);
        //Braga - Braga
        assertTrue(
                "then 60 (Porto - Braga) + 75 (Porto - Aveiro) = 135 (Braga - Aveiro)",
                itEdges.next() == 135);
        assertTrue(
                "then 60 (Porto - Braga) + 135 (Porto - Coimbra) = 195 (Braga - Coimbra)",
                itEdges.next() == 195);
    }

}
