/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import libs.adjacency.AdjacencyMatrixGraph;
import libs.mapgraph.Graph;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Vitor Melo
 */
public class GameTest {

    public GameTest() {
    }

    /**
     * Test of lowerDifficultyPath method, of class Game.
     */
    @Test
    public void testLowerDifficultyPath() {
        System.out.println("lowerDifficultyPath");
        AdjacencyMatrixGraph<Local, Estrada> graph = new AdjacencyMatrixGraph<>();

        Local a = new Local("A", 0);
        Local b = new Local("B", 0);
        Local c = new Local("C", 0);
        Local d = new Local("D", 0);

        Estrada ab = new Estrada(30);
        Estrada ac = new Estrada(30);
        Estrada ad = new Estrada(50);
        Estrada bd = new Estrada(5);
        Estrada cd = new Estrada(10);

        graph.insertVertex(a);
        graph.insertVertex(b);
        graph.insertVertex(c);
        graph.insertVertex(d);

        graph.insertEdge(a, b, ab);
        graph.insertEdge(a, c, ac);
        graph.insertEdge(a, d, ad);
        graph.insertEdge(b, d, bd);
        graph.insertEdge(c, d, cd);

        Local orig = a;
        Local dest = d;
        LinkedList<Local> expResult = new LinkedList<>();
        expResult.add(a);
        expResult.add(b);
        expResult.add(d);
        Game instance = new Game();
        instance.setMapaTerreno(graph);
        LinkedList<Local> result = instance.lowerDifficultyPath(orig, dest);
        assertEquals(expResult, result);
    }

    /**
     * Test of lowerDifficultyPath method, of class Game.
     */
    @Test
    public void testLowerDifficultyPath1() {
        System.out.println("lowerDifficultyPath1");
        AdjacencyMatrixGraph<Local, Estrada> graph = new AdjacencyMatrixGraph<>();

        Local a = new Local("A", 1);
        Local b = new Local("B", 7);
        Local c = new Local("C", 2);
        Local d = new Local("D", 4);
        Local e = new Local("E", 10);

        Personagem p1 = new Personagem("p1", 20);
        Personagem p2 = new Personagem("p2", 10);

        a.setDono(p1);
        e.setDono(p2);

        Estrada ab = new Estrada(30);
        Estrada ac = new Estrada(30);
        Estrada ad = new Estrada(50);
        Estrada bd = new Estrada(5);
        Estrada cd = new Estrada(10);
        Estrada ce = new Estrada(5);
        Estrada de = new Estrada(5);

        graph.insertVertex(a);
        graph.insertVertex(b);
        graph.insertVertex(c);
        graph.insertVertex(d);
        graph.insertVertex(e);

        graph.insertEdge(a, b, ab);
        graph.insertEdge(a, c, ac);
        graph.insertEdge(a, d, ad);
        graph.insertEdge(b, d, bd);
        graph.insertEdge(c, d, cd);
        graph.insertEdge(c, e, ce);
        graph.insertEdge(d, e, de);

        Local orig = a;
        Local dest = e;
        LinkedList<Local> expResult = new LinkedList<>();
        expResult.add(a);
        expResult.add(c);
        expResult.add(e);
        Game instance = new Game();
        instance.setMapaTerreno(graph);
        LinkedList<Local> result = instance.lowerDifficultyPath(orig, dest);
        assertEquals(expResult, result);
    }

    /**
     * Test of lowerDifficultyPath method, of class Game.
     */
    @Test
    public void testLowerDifficultyPath2() {
        System.out.println("lowerDifficultyPath2");
        AdjacencyMatrixGraph<Local, Estrada> graph = new AdjacencyMatrixGraph<>();

        Local a = new Local("A", 1);
        Local b = new Local("B", 7);
        Local c = new Local("C", 2);
        Local d = new Local("D", 4);
        Local e = new Local("E", 10);

        Personagem p1 = new Personagem("p1", 20);
        Personagem p2 = new Personagem("p2", 10);

        a.setDono(p1);
        e.setDono(p2);

        Estrada ab = new Estrada(5);
        Estrada ac = new Estrada(30);
        Estrada ad = new Estrada(50);
        Estrada bd = new Estrada(5);
        Estrada cd = new Estrada(5);
        Estrada ce = new Estrada(5);
        Estrada de = new Estrada(10);

        graph.insertVertex(a);
        graph.insertVertex(b);
        graph.insertVertex(c);
        graph.insertVertex(d);
        graph.insertVertex(e);

        graph.insertEdge(a, b, ab);
        graph.insertEdge(a, c, ac);
        graph.insertEdge(a, d, ad);
        graph.insertEdge(b, d, bd);
        graph.insertEdge(c, d, cd);
        graph.insertEdge(c, e, ce);
        graph.insertEdge(d, e, de);

        Local orig = a;
        Local dest = e;
        LinkedList<Local> expResult = new LinkedList<>();
        expResult.add(a);
        expResult.add(b);
        expResult.add(d);
        expResult.add(e);
        Game instance = new Game();
        instance.setMapaTerreno(graph);
        LinkedList<Local> result = instance.lowerDifficultyPath(orig, dest);
        assertEquals(expResult, result);
    }

    /**
     * Test of alliesPer method, of class Game. Test 2-B
     */
    @Test
    public void testAliadosPersonagem() {
        System.out.println("alliesPer");
        Graph<Personagem, Alianca> graph = new Graph<>(false);

        Personagem p1 = new Personagem("p1", 30);
        Personagem p2 = new Personagem("p2", 62);
        Personagem p3 = new Personagem("p3", 20);
        Personagem p4 = new Personagem("p4", 50);
        Personagem p5 = new Personagem("p5", 75);

        graph.insertVertex(p1);
        graph.insertVertex(p2);
        graph.insertVertex(p3);
        graph.insertVertex(p4);
        graph.insertVertex(p5);

        Alianca a1 = new Alianca(true, 0.5f, p1, p2);
        Alianca a2 = new Alianca(false, 0.3f, p1, p3);
        Alianca a3 = new Alianca(true, 0.6f, p1, p4);

        graph.insertEdge(p1, p2, a1, 1);
        graph.insertEdge(p1, p3, a2, 1);
        graph.insertEdge(p1, p4, a3, 1);

        Game instance = new Game();
        instance.setAliancaPersonagens(graph);

        List<Personagem> expResult = new ArrayList<>();
        expResult.add(p2);
        expResult.add(p3);
        expResult.add(p4);

        List<Personagem> result = instance.aliadosPersonagem(p1);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAliancaMaisForte method, of class Game.
     */
    @Test
    public void testGetAliancaMaisForte() {
        System.out.println("getAliancaMaisForte");

        Graph<Personagem, Alianca> graph = new Graph<>(false);

        Personagem p1 = new Personagem("p1", 30);
        Personagem p2 = new Personagem("p2", 62);
        Personagem p3 = new Personagem("p3", 20);
        Personagem p4 = new Personagem("p4", 50);
        Personagem p5 = new Personagem("p5", 75);

        graph.insertVertex(p1);
        graph.insertVertex(p2);
        graph.insertVertex(p3);
        graph.insertVertex(p4);
        graph.insertVertex(p5);

        Alianca a1 = new Alianca(true, 0.5f, p1, p2);
        Alianca a2 = new Alianca(false, 0.3f, p1, p3);
        Alianca a3 = new Alianca(true, 0.6f, p1, p4);

        graph.insertEdge(p1, p2, a1, 1);
        graph.insertEdge(p1, p3, a2, 1);
        graph.insertEdge(p1, p4, a3, 1);

        Game instance = new Game();
        instance.setAliancaPersonagens(graph);

        Personagem[] pers = new Personagem[2];
        pers[0] = a3.getPer1();
        pers[1] = a3.getPer2();

        Map<Float, Personagem[]> expResult = new HashMap();
        expResult.put(a3.calculateForca(), pers);
        instance.setAliancaPersonagens(graph);
        Map<Float, Personagem[]> result = instance.getAliancaMaisForte();

        for (Map.Entry<Float, Personagem[]> entry : result.entrySet()) {
            for (Map.Entry<Float, Personagem[]> entry1 : expResult.entrySet()) {
                assertEquals(entry.getKey(), entry1.getKey());
                Assert.assertArrayEquals(entry.getValue(), entry1.getValue());
            }
        }
    }

    /**
     * Test of pathToConquer method, of class Game.
     *
     * Teste 2 em 1, verifica o valor do caminho e o próprio caminho
     */
    @Test
    public void testPathToConquer() {
        System.out.println("pathToConquer");

        AdjacencyMatrixGraph<Local, Estrada> graph = new AdjacencyMatrixGraph<>();

        Local a = new Local("A", 1);
        Local b = new Local("B", 7);
        Local c = new Local("C", 2);
        Local d = new Local("D", 4);
        Local e = new Local("E", 10);

        Personagem p1 = new Personagem("p1", 20);
        Personagem p2 = new Personagem("p2", 10);

        a.setDono(p1);
        e.setDono(p2);

        Estrada ab = new Estrada(5);
        Estrada ac = new Estrada(30);
        Estrada ad = new Estrada(50);
        Estrada bd = new Estrada(5);
        Estrada cd = new Estrada(5);
        Estrada ce = new Estrada(5);
        Estrada de = new Estrada(10);

        graph.insertVertex(a);
        graph.insertVertex(b);
        graph.insertVertex(c);
        graph.insertVertex(d);
        graph.insertVertex(e);

        graph.insertEdge(a, b, ab);
        graph.insertEdge(a, c, ac);
        graph.insertEdge(a, d, ad);
        graph.insertEdge(b, d, bd);
        graph.insertEdge(c, d, cd);
        graph.insertEdge(c, e, ce);
        graph.insertEdge(d, e, de);

        Local dest = e;
        Game instance = new Game();
        Map<LinkedList<Local>, Double> bestPathMap = new HashMap<>();
        Map<LinkedList<Local>, Double> pathMap = new HashMap<>();

        LinkedList<Local> bestPath = new LinkedList<>();
        bestPath.add(a);
        bestPath.add(b);
        bestPath.add(d);
        bestPath.add(e);
        double expResult = 51;
        bestPathMap.put(bestPath, expResult);

        boolean expRes = false;
        instance.setMapaTerreno(graph);
        boolean result = instance.canPerConquer(dest, p1, pathMap);

        Assert.assertEquals(pathMap, bestPathMap);
        Assert.assertTrue(result == expRes);

    }

    /**
     * Test of pathToConquer method, of class Game.
     *
     * Teste 2 em 1, verifica o valor do caminho e o próprio caminho
     */
    @Test
    public void testPathToConquer2() {
        System.out.println("pathToConquer2");

        AdjacencyMatrixGraph<Local, Estrada> graph = new AdjacencyMatrixGraph<>();

        Local a = new Local("A", 1);
        Local b = new Local("B", 7);
        Local c = new Local("C", 2);
        Local d = new Local("D", 4);
        Local e = new Local("E", 10);

        Personagem p1 = new Personagem("p1", 300);
        Personagem p2 = new Personagem("p2", 10);

        a.setDono(p1);
        e.setDono(p2);

        Estrada ab = new Estrada(5);
        Estrada ac = new Estrada(30);
        Estrada ad = new Estrada(50);
        Estrada bd = new Estrada(5);
        Estrada cd = new Estrada(5);
        Estrada ce = new Estrada(5);
        Estrada de = new Estrada(10);

        graph.insertVertex(a);
        graph.insertVertex(b);
        graph.insertVertex(c);
        graph.insertVertex(d);
        graph.insertVertex(e);

        graph.insertEdge(a, b, ab);
        graph.insertEdge(a, c, ac);
        graph.insertEdge(a, d, ad);
        graph.insertEdge(b, d, bd);
        graph.insertEdge(c, d, cd);
        graph.insertEdge(c, e, ce);
        graph.insertEdge(d, e, de);

        Local dest = e;
        Game instance = new Game();
        Map<LinkedList<Local>, Double> bestPathMap = new HashMap<>();
        Map<LinkedList<Local>, Double> pathMap = new HashMap<>();

        LinkedList<Local> bestPath = new LinkedList<>();
        bestPath.add(a);
        bestPath.add(b);
        bestPath.add(d);
        bestPath.add(e);
        double expResult = 51;
        bestPathMap.put(bestPath, expResult);

        boolean expRes = true;
        instance.setMapaTerreno(graph);
        boolean result = instance.canPerConquer(dest, p1, pathMap);

        Assert.assertEquals(pathMap, bestPathMap);
        Assert.assertTrue(result == expRes);

    }

    /**
     * Test of canAlliesConquer method, of class Game.
     */
    @Test
    public void testCanAlliesConquer() {
        System.out.println("canAlliesConquer");

        AdjacencyMatrixGraph<Local, Estrada> graph = new AdjacencyMatrixGraph<>();

        Local a = new Local("A", 1);
        Local b = new Local("B", 7);
        Local c = new Local("C", 2);
        Local d = new Local("D", 4);
        Local e = new Local("E", 10);

        Personagem p1 = new Personagem("p1", 20);
        Personagem p2 = new Personagem("p2", 10);
        Personagem p3 = new Personagem("p3", 40);
        Personagem p4 = new Personagem("p4", 112);

        a.setDono(p1);
        e.setDono(p2);
        d.setDono(p3);
        c.setDono(p4);

        Estrada ab = new Estrada(5);
        Estrada ac = new Estrada(30);
        Estrada ad = new Estrada(50);
        Estrada bd = new Estrada(5);
        Estrada cd = new Estrada(5);
        Estrada ce = new Estrada(5);
        Estrada de = new Estrada(10);

        graph.insertVertex(a);
        graph.insertVertex(b);
        graph.insertVertex(c);
        graph.insertVertex(d);
        graph.insertVertex(e);

        graph.insertEdge(a, b, ab);
        graph.insertEdge(a, c, ac);
        graph.insertEdge(a, d, ad);
        graph.insertEdge(b, d, bd);
        graph.insertEdge(c, d, cd);
        graph.insertEdge(c, e, ce);
        graph.insertEdge(d, e, de);

        Map<LinkedList<Local>, Double> bestPathMap = new HashMap<>();
        Map<LinkedList<Local>, Double> pathMap = new HashMap<>();

        LinkedList<Local> bestPath = new LinkedList<>();
        bestPath.add(a);
        bestPath.add(b);
        bestPath.add(d);
        bestPath.add(e);
        double expResult = 131;
        bestPathMap.put(bestPath, expResult);

        Graph<Personagem, Alianca> aliancas = new Graph<>(false);

        aliancas.insertVertex(p1);
        aliancas.insertVertex(p2);
        aliancas.insertVertex(p3);
        aliancas.insertVertex(p4);

        Alianca a1 = new Alianca(true, 0.5f, p2, p4);
        Alianca a2 = new Alianca(true, 0.4f, p1, p3);
        Alianca a3 = new Alianca(true, 1, p1, p4);
        Alianca a4 = new Alianca(true, 0.5f, p3, p4);

        aliancas.insertEdge(p2, p4, a1, 0.5f);
        aliancas.insertEdge(p1, p3, a2, 0.4f);
        aliancas.insertEdge(p1, p4, a3, 1);
        aliancas.insertEdge(p3, p4, a4, 0.5f);

        Personagem expRes = p4;
        Game instance = new Game();
        instance.setMapaTerreno(graph);
        instance.setAliancaPersonagens(aliancas);
        Local dest = e;
        Personagem result = instance.canAlliesConquer(p1, dest, pathMap);

        assertEquals(expRes, result);
        assertEquals(bestPathMap, pathMap);

    }

    /**
     * Test of canAlliesConquer method, of class Game.
     */
    @Test
    public void testCanAlliesConquer2() {
        System.out.println("canAlliesConquer2");

        AdjacencyMatrixGraph<Local, Estrada> graph = new AdjacencyMatrixGraph<>();

        Local a = new Local("A", 1);
        Local b = new Local("B", 7);
        Local c = new Local("C", 2);
        Local d = new Local("D", 4);
        Local e = new Local("E", 10);

        Personagem p1 = new Personagem("p1", 20);
        Personagem p2 = new Personagem("p2", 10);
        Personagem p3 = new Personagem("p3", 40);
        Personagem p4 = new Personagem("p4", 112);

        a.setDono(p1);
        e.setDono(p2);
        d.setDono(p3);
        c.setDono(p4);

        Estrada ab = new Estrada(5);
        Estrada ac = new Estrada(30);
        Estrada ad = new Estrada(50);
        Estrada bd = new Estrada(5);
        Estrada cd = new Estrada(5);
        Estrada ce = new Estrada(5);
        Estrada de = new Estrada(10);

        graph.insertVertex(a);
        graph.insertVertex(b);
        graph.insertVertex(c);
        graph.insertVertex(d);
        graph.insertVertex(e);

        graph.insertEdge(a, b, ab);
        graph.insertEdge(a, c, ac);
        graph.insertEdge(a, d, ad);
        graph.insertEdge(b, d, bd);
        graph.insertEdge(c, d, cd);
        graph.insertEdge(c, e, ce);
        graph.insertEdge(d, e, de);

        Map<LinkedList<Local>, Double> bestPathMap = new HashMap<>();
        Map<LinkedList<Local>, Double> pathMap = new HashMap<>();

        LinkedList<Local> bestPath = new LinkedList<>();
        bestPath.add(a);
        bestPath.add(b);
        bestPath.add(d);
        bestPath.add(e);
        double expResult = 131;
        bestPathMap.put(bestPath, expResult);

        Graph<Personagem, Alianca> aliancas = new Graph<>(false);

        aliancas.insertVertex(p1);
        aliancas.insertVertex(p2);
        aliancas.insertVertex(p3);
        aliancas.insertVertex(p4);

        Alianca a1 = new Alianca(true, 0.5f, p2, p4);
        Alianca a2 = new Alianca(true, 0.4f, p1, p3);
        Alianca a3 = new Alianca(true, 0.5f, p1, p4);
        Alianca a4 = new Alianca(true, 0.5f, p3, p4);

        aliancas.insertEdge(p2, p4, a1, 0.5f);
        aliancas.insertEdge(p1, p3, a2, 0.4f);
        aliancas.insertEdge(p1, p4, a3, 0.5f);
        aliancas.insertEdge(p3, p4, a4, 0.5f);

        Personagem expRes = null;
        Game instance = new Game();
        instance.setMapaTerreno(graph);
        instance.setAliancaPersonagens(aliancas);
        Local dest = e;
        Personagem result = instance.canAlliesConquer(p1, dest, pathMap);

        assertEquals(expRes, result);
        assertEquals(bestPathMap, pathMap);

    }

    /**
     * Test of newAlianca method, of class Game. Aliança já existente
     * previamente.
     */
    @Test
    public void testNewAlianca1() {
        System.out.println("newAlianca1");
        Graph<Personagem, Alianca> graph = new Graph<>(false);

        Personagem perA = new Personagem("Vitor", 30);
        Personagem perB = new Personagem("Filipe", 30);
        graph.insertVertex(perA);
        graph.insertVertex(perB);

        Alianca a1 = new Alianca(true, 1, perA, perB);
        graph.insertEdge(perA, perB, a1, 1);

        
        Graph<Personagem, Alianca> graphResult = graph.clone();
        
        Alianca a2 = new Alianca(true, 0.8f, perA, perB);
        graphResult.insertEdge(perA, perB, a2, 0.8f);
        
        Game instance = new Game();
        instance.setAliancaPersonagens(graph);
        boolean expResult = true;
        boolean result = instance.newAlianca(perA, perB);
        assertEquals(graphResult, instance.getAliancaPersonagens());
        assertEquals(expResult, result);

    }

    /**
     * Test of newAlianca method, of class Game. Mesma rede de alianças
     */
    @Test
    public void testNewAlianca2() {
        System.out.println("newAlianca2");
        Graph<Personagem, Alianca> graph = new Graph<>(false);

        Personagem perA = new Personagem("Vitor", 30);
        Personagem perB = new Personagem("Filipe", 30);
        Personagem perC = new Personagem("João", 30);
        graph.insertVertex(perA);
        graph.insertVertex(perB);
        graph.insertVertex(perC);

        Alianca a1 = new Alianca(true, 1, perA, perC);
        Alianca a2 = new Alianca(true, 1, perC, perB);
        graph.insertEdge(perA, perC, a1, 1);
        graph.insertEdge(perC, perB, a2, 1);

        Graph<Personagem, Alianca> graphResult = graph.clone();
        
        Alianca a3 = new Alianca(true, 0.8f, perA, perB);
        graphResult.insertEdge(perA, perB, a3, 0.8f);
        
        Game instance = new Game();
        instance.setAliancaPersonagens(graph);
        boolean expResult = true;
        boolean result = instance.newAlianca(perA, perB);
        assertEquals(graphResult, instance.getAliancaPersonagens());
        assertEquals(expResult, result);
    }

    /**
     * Test of newAlianca method, of class Game. Personagem não existe na rede
     * de alianças
     */
    @Test
    public void testNewAlianca3() {
        System.out.println("newAlianca3");
        Graph<Personagem, Alianca> graph = new Graph<>(false);

        Personagem perA = new Personagem("Vitor", 30);
        Personagem perB = new Personagem("Filipe", 30);
        Personagem perC = new Personagem("João", 30);
        Personagem perD = new Personagem("Edu", 30);
        graph.insertVertex(perA);
        graph.insertVertex(perB);
        graph.insertVertex(perC);
        graph.insertVertex(perD);
        
        Alianca a1 = new Alianca(true, 1, perA, perB);
        graph.insertEdge(perA, perB, a1, 1);

        Alianca a2 = new Alianca(true, 1, perC, perD);
        graph.insertEdge(perC, perD, a2, 1);
        
        Graph<Personagem, Alianca> graphResult = graph.clone();
        
        Alianca a3 = new Alianca(true, 0.8f, perB, perC);
        graphResult.insertEdge(perB, perC, a3, 0.8f);
        
        Game instance = new Game();
        instance.setAliancaPersonagens(graph);
        boolean expResult = true;
        boolean result = instance.newAlianca(perB, perC);
        assertEquals(graphResult, instance.getAliancaPersonagens());
        assertEquals(expResult, result);

    }

    /**
     * Test of getPossibleAlliances method, of class Game.
     */
    @Test
    public void testGetPossibleAlliances() {
        System.out.println("getPossibleAlliances");

        Personagem p1 = new Personagem("p1", 20);
        Personagem p2 = new Personagem("p2", 10);
        Personagem p3 = new Personagem("p3", 40);
        Personagem p4 = new Personagem("p4", 112);

        Graph<Personagem, Alianca> aliancas = new Graph<>(false);

        aliancas.insertVertex(p1);
        aliancas.insertVertex(p2);
        aliancas.insertVertex(p3);
        aliancas.insertVertex(p4);

        Alianca a1 = new Alianca(true, 0.5f, p2, p4);
        Alianca a2 = new Alianca(true, 0.4f, p1, p3);
        Alianca a3 = new Alianca(true, 0.5f, p3, p4);

        aliancas.insertEdge(p2, p4, a1, 0.5f);
        aliancas.insertEdge(p1, p3, a2, 0.4f);
        aliancas.insertEdge(p3, p4, a3, 0.5f);

        Graph<Personagem, Alianca> aliancasRes = new Graph<>(false);

        aliancasRes.insertVertex(p1);
        aliancasRes.insertVertex(p2);
        aliancasRes.insertVertex(p3);
        aliancasRes.insertVertex(p4);

        Alianca a4 = new Alianca(true, 0.8f, p1, p2);
        Alianca a5 = new Alianca(true, 0.8f, p1, p4);
        Alianca a6 = new Alianca(true, 0.8f, p2, p3);

        aliancasRes.insertEdge(p1, p2, a4, 0.8f);
        aliancasRes.insertEdge(p1, p4, a5, 0.8f);
        aliancasRes.insertEdge(p2, p3, a6, 0.8f);

        Game instance = new Game();
        Game expResultInstance = new Game();
        Game resultInstance = new Game();
        instance.setAliancaPersonagens(aliancas);
        expResultInstance.setAliancaPersonagens(aliancasRes);

        Graph<Personagem, Alianca> result = instance.getPossibleAlliances();

        resultInstance.setAliancaPersonagens(result);

        for (Personagem vertice : resultInstance.getAliancaPersonagens().
                vertices()) {
            assertEquals(resultInstance.aliadosPersonagem(vertice),
                    expResultInstance.aliadosPersonagem(vertice));
        }
    }

    /**
     * Test of getPossibleAlliances method, of class Game.
     */
    @Test
    public void testGetPossibleAlliances2() {
        System.out.println("getPossibleAlliances2");

        Personagem p1 = new Personagem("p1", 20);
        Personagem p2 = new Personagem("p2", 10);
        Personagem p3 = new Personagem("p3", 40);
        Personagem p4 = new Personagem("p4", 112);

        Graph<Personagem, Alianca> aliancas = new Graph<>(false);

        aliancas.insertVertex(p1);
        aliancas.insertVertex(p2);
        aliancas.insertVertex(p3);
        aliancas.insertVertex(p4);

        Alianca a1 = new Alianca(true, 0.5f, p1, p4);
        Alianca a2 = new Alianca(true, 0.4f, p1, p2);
        Alianca a3 = new Alianca(true, 0.5f, p1, p3);
        Alianca a4 = new Alianca(true, 0.5f, p2, p3);
        Alianca a5 = new Alianca(true, 0.5f, p2, p3);
        Alianca a6 = new Alianca(true, 0.5f, p3, p4);

        aliancas.insertEdge(p1, p4, a1, 0.5f);
        aliancas.insertEdge(p1, p2, a2, 0.5f);
        aliancas.insertEdge(p1, p3, a3, 0.5f);
        aliancas.insertEdge(p2, p3, a4, 0.5f);
        aliancas.insertEdge(p2, p4, a5, 0.5f);
        aliancas.insertEdge(p3, p4, a6, 0.5f);

        Graph<Personagem, Alianca> aliancasRes = new Graph<>(false);

        aliancasRes.insertVertex(p1);
        aliancasRes.insertVertex(p2);
        aliancasRes.insertVertex(p3);
        aliancasRes.insertVertex(p4);

        Game instance = new Game();
        Game expResultInstance = new Game();
        Game resultInstance = new Game();
        instance.setAliancaPersonagens(aliancas);
        expResultInstance.setAliancaPersonagens(aliancasRes);

        Graph<Personagem, Alianca> result = instance.getPossibleAlliances();
        resultInstance.setAliancaPersonagens(result);
        for (Personagem vertice : resultInstance.getAliancaPersonagens().
                vertices()) {
            assertEquals(resultInstance.aliadosPersonagem(vertice),
                    expResultInstance.aliadosPersonagem(vertice));
        }
    }

}
