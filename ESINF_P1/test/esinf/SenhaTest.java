/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Vitor Melo
 */
public class SenhaTest {
    
    public SenhaTest() {
    }

    /**
     * Test of equals method, of class Senha.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new Senha(0, "A");
        Senha instance = new Senha(0, "A");
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class Senha.
     */
    @Test
    public void testEquals1() {
        System.out.println("equals");
        Object obj = new Senha(0, "B");
        Senha instance = new Senha(0, "A");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Senha.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Senha instance = new Senha(0, "A");
        int expResult = 65928;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }
    
}
