/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Vitor Melo
 */
public class FinancasTest {

    private final Financas instance;

    public FinancasTest() throws IOException {
        List<String> rep = Files.lines(Paths.get("fx_reparticoes.txt"))
                .collect(Collectors.toList());
        List<String> sen = Files.lines(Paths.get("fx_senhas.txt"))
                .collect(Collectors.toList());
        List<String> cid = Files.lines(Paths.get("fx_cidadaos.txt"))
                .collect(Collectors.toList());
        instance = new Financas();
        instance.getFilesInfo(rep, cid, sen);
    }

    /**
     * Test of getFilesInfo method, of class Financas.
     */
    @Test
    public void testGetFilesInfo() {
        System.out.println("getFilesInfo");
        Reparticao r1 = new Reparticao("Porto", 1234, "4200");
        Reparticao r2 = new Reparticao("Maia", 1235, "4470");
        HashSet<String> r1serv = new HashSet<>();
        r1serv.add("A");
        r1serv.add("B");
        r1serv.add("C");
        r1.setServicos(r1serv);
        HashSet<String> r2serv = new HashSet<>();
        r2serv.add("A");
        r2serv.add("B");
        r2.setServicos(r2serv);
        Cidadao c1 = new Cidadao(111222333, "Ana", "ana@gmail.com", "4200-072");
        Cidadao c2 = new Cidadao(223344, "Berta", "berta@gmail.com", "4200-071");
        Cidadao c3 = new Cidadao(584769, "Manuel", "manuel@gmail.com",
                "4715-357");
        Senha s1 = new Senha(1, "A");
        Senha s2 = new Senha(1, "C");
        Senha s3 = new Senha(2, "A");
        Senha s4 = new Senha(3, "A");
        Senha s5 = new Senha(4, "C");
        DoublyLinkedList<Reparticao> dll = new DoublyLinkedList<>();
        r1.insertNewCidadao(c1);
        r1.insertNewCidadao(c2);
        r1.addCidadaoSenha(c1, s1);
        r1.addCidadaoSenha(c1, s2);
        r1.addCidadaoSenha(c2, s5);
        dll.addLast(r1);
        dll.addLast(r2);
        assertEquals(dll, instance.getDll());
    }

    /**
     * Test of removeReparticao method, of class Financas.
     */
    @Test
    public void testRemoveReparticao() {
        System.out.println("removeReparticao");
        String codPostal = "4470";
        instance.removeReparticao(codPostal);

        Reparticao r1 = new Reparticao("Porto", 1234, "4200");
        Reparticao r2 = new Reparticao("Maia", 1235, "4470");
        HashSet<String> r1serv = new HashSet<>();
        r1serv.add("A");
        r1serv.add("B");
        r1serv.add("C");
        r1.setServicos(r1serv);
        HashSet<String> r2serv = new HashSet<>();
        r2serv.add("A");
        r2serv.add("B");
        r2.setServicos(r2serv);
        Cidadao c1 = new Cidadao(111222333, "Ana", "ana@gmail.com", "4200-072");
        Cidadao c2 = new Cidadao(223344, "Berta", "berta@gmail.com", "4200-071");
        Cidadao c3 = new Cidadao(584769, "Manuel", "manuel@gmail.com",
                "4715-357");
        Senha s1 = new Senha(1, "A");
        Senha s2 = new Senha(1, "C");
        Senha s3 = new Senha(2, "A");
        Senha s4 = new Senha(3, "A");
        Senha s5 = new Senha(4, "C");
        DoublyLinkedList<Reparticao> dll = new DoublyLinkedList<>();
        r1.insertNewCidadao(c1);
        r1.insertNewCidadao(c2);
        r1.addCidadaoSenha(c1, s1);
        r1.addCidadaoSenha(c1, s2);
        r1.addCidadaoSenha(c2, s5);
        dll.addLast(r1);
        assertEquals(dll, instance.getDll());
    }

    /**
     * Test of addNewCidadaos method, of class Financas.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testAddNewCidadaos() throws IOException {
        System.out.println("addNewCidadaos");
        List<String> cidadaos = Files.lines(Paths.get("fx_novoscidadaos.txt"))
                .collect(Collectors.toList());
        instance.addNewCidadaos(cidadaos);

        Reparticao r1 = new Reparticao("Porto", 1234, "4200");
        Reparticao r2 = new Reparticao("Maia", 1235, "4470");
        HashSet<String> r1serv = new HashSet<>();
        r1serv.add("A");
        r1serv.add("B");
        r1serv.add("C");
        r1.setServicos(r1serv);
        HashSet<String> r2serv = new HashSet<>();
        r2serv.add("A");
        r2serv.add("B");
        r2.setServicos(r2serv);
        Cidadao c1 = new Cidadao(111222333, "Ana", "ana@gmail.com", "4200-072");
        Cidadao c2 = new Cidadao(223344, "Berta", "berta@gmail.com", "4200-071");
        Cidadao c3 = new Cidadao(584769, "Manuel", "manuel@gmail.com",
                "4715-357");
        Cidadao c4 = new Cidadao(223344, "Vitor", "vitor@gmail.com", "4200-075");
        Cidadao c5 = new Cidadao(223344, "Joao", "joao@gmail.com", "4200-076");
        Senha s1 = new Senha(1, "A");
        Senha s2 = new Senha(1, "C");
        Senha s3 = new Senha(2, "A");
        Senha s4 = new Senha(3, "A");
        Senha s5 = new Senha(4, "C");
        DoublyLinkedList<Reparticao> dll = new DoublyLinkedList<>();
        r1.insertNewCidadao(c1);
        r1.insertNewCidadao(c2);
        r1.insertNewCidadao(c4);
        r1.insertNewCidadao(c5);
        r1.addCidadaoSenha(c1, s1);
        r1.addCidadaoSenha(c1, s2);
        r1.addCidadaoSenha(c2, s5);
        dll.addLast(r1);
        dll.addLast(r2);
        assertEquals(dll, instance.getDll());
    }

    /**
     * Test of addNewReparticao method, of class Financas.
     * @throws java.io.IOException
     */
    @Test
    public void testAddNewReparticao() throws IOException {
        System.out.println("addNewReparticao");
        
        List<String> cidadaos = Files.lines(Paths.get("fx_novoscidadaos2.txt"))
                .collect(Collectors.toList());
        List<String> reparticoes = Files.lines(Paths.get("fx_novasreparticoes.txt"))
                .collect(Collectors.toList());

        Reparticao r1 = new Reparticao("Porto", 1234, "4200");
        Reparticao r2 = new Reparticao("Maia", 1235, "4470");
        Reparticao r3 = new Reparticao("SJM", 3200, "3700");
        Reparticao r4 = new Reparticao("Gaia", 5600, "4100");
        
        HashSet<String> r1serv = new HashSet<>();
        r1serv.add("A");
        r1serv.add("B");
        r1serv.add("C");
        r1.setServicos(r1serv);
        HashSet<String> r2serv = new HashSet<>();
        r2serv.add("A");
        r2serv.add("B");
        HashSet<String> r3serv = new HashSet<>();
        r3serv.add("A");
        r3serv.add("C");
        r3.setServicos(r3serv);
        HashSet<String> r4serv = new HashSet<>();
        r4serv.add("A");
        r4serv.add("Z");
        r4.setServicos(r4serv);
        
        Cidadao c1 = new Cidadao(111222333, "Ana", "ana@gmail.com", "4200-072");
        Cidadao c2 = new Cidadao(223344, "Berta", "berta@gmail.com", "4200-071");
        Cidadao c3 = new Cidadao(584769, "Manuel", "manuel@gmail.com",
                "4715-357");
        Senha s1 = new Senha(1, "A");
        Senha s2 = new Senha(1, "C");
        Senha s3 = new Senha(2, "A");
        Senha s4 = new Senha(3, "A");
        Senha s5 = new Senha(4, "C");
        DoublyLinkedList<Reparticao> dll = new DoublyLinkedList<>();
        r1.insertNewCidadao(c1);
        r1.insertNewCidadao(c2);
        r1.addCidadaoSenha(c1, s1);
        r1.addCidadaoSenha(c1, s2);
        r1.addCidadaoSenha(c2, s5);
        dll.addLast(r1);
        dll.addLast(r2);
        dll.addLast(r3);
        
        instance.addNewReparticao(reparticoes, cidadaos);
    }

    /**
     * Test of getReparticaoData method, of class Financas.
     */
    @Test
    public void testGetReparticaoData() {
        System.out.println("getReparticaoData");
        Map<String, Set<Integer>> expResult = new HashMap<>();
        Set<Integer> nContrib1 = new HashSet<>();
        nContrib1.add(111222333);
        nContrib1.add(223344);
        expResult.put("1234 - Porto", nContrib1);
        expResult.put("1235 - Maia", new HashSet<>());
        Map<String, Set<Integer>> result = instance.getReparticaoData();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRepaticaoUsage method, of class Financas.
     */
    @Test
    public void testGetRepaticaoUsage() {
        System.out.println("getRepaticaoUsage");
        
        Financas f = new Financas();
        Reparticao r1 = new Reparticao("Porto", 1234, "4200");
        HashSet<String> r1serv = new HashSet<>();
        r1serv.add("A");
        r1serv.add("B");
        r1serv.add("C");
        r1.setServicos(r1serv);

        Cidadao c1 = new Cidadao(111222333, "Ana", "ana@gmail.com", "4200-072");
        Cidadao c2 = new Cidadao(223344, "Berta", "berta@gmail.com", "4200-071");
        Cidadao c3 = new Cidadao(584769, "Carlos", "carlos@gmail.com",
                "4200-073");
        Senha s1 = new Senha(1, "A");
        Senha s2 = new Senha(1, "B");
        Senha s3 = new Senha(1, "C");
        Senha s4 = new Senha(2, "A");
        Senha s5 = new Senha(2, "B");
        Senha s6 = new Senha(2, "C");
        Senha s7 = new Senha(3, "A");
        Senha s8 = new Senha(3, "B");
        Senha s9 = new Senha(3, "C");
        r1.insertNewCidadao(c1);
        r1.insertNewCidadao(c2);
        r1.insertNewCidadao(c3);
        r1.addCidadaoSenha(c1, s1);
        r1.addCidadaoSenha(c1, s2);
        r1.addCidadaoSenha(c1, s3);
        r1.addCidadaoSenha(c2, s5);
        r1.addCidadaoSenha(c2, s9);
        r1.addCidadaoSenha(c2, s7);
        r1.addCidadaoSenha(c3, s4);
        r1.addCidadaoSenha(c3, s8);
        r1.addCidadaoSenha(c3, s6);
        
        
        Map<String, LinkedHashSet<Cidadao>> expResult = new TreeMap<>();
        
        expResult.put("A", new LinkedHashSet<>());
        expResult.put("B", new LinkedHashSet<>());
        expResult.put("C", new LinkedHashSet<>());
        expResult.get("A").add(c1);
        expResult.get("A").add(c3);
        expResult.get("A").add(c2);
        expResult.get("B").add(c2);
        expResult.get("B").add(c1);
        expResult.get("B").add(c3);
        expResult.get("C").add(c3);
        expResult.get("C").add(c2);
        expResult.get("C").add(c1);
        
        Map<String, LinkedHashSet<Cidadao>> result = f.getRepaticaoUsage(r1, 0, 30);
        assertEquals(null, result);
    }

}
