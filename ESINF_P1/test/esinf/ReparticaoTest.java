/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import static junit.framework.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Vitor Melo
 */
public class ReparticaoTest {

    private final Reparticao instance;

    public ReparticaoTest() throws IOException {
        instance = new Reparticao("Porto", 1234, "4200");
        List<String> cid = Files.lines(Paths.get("fx_cidadaos.txt")).collect(
                Collectors.toList());
        List<String> sen = Files.lines(Paths.get("fx_senhas.txt")).collect(
                Collectors.toList());
        instance.readAllCidadaosSenhas(cid, sen);
    }

    /**
     * Test of abandonQueue method, of class Reparticao.
     */
    @Test
    public void testAbandonQueue() {
        System.out.println("abandonQueue");
        Cidadao c = new Cidadao(111222333, "Ana", "ana@gmail.com", "4200-072");
        instance.abandonQueue(c);
        Reparticao r1 = new Reparticao("Porto", 1234, "4200");
        HashSet<String> r1serv = new HashSet<>();
        r1serv.add("A");
        r1serv.add("B");
        r1serv.add("C");
        r1.setServicos(r1serv);
        Cidadao c1 = new Cidadao(111222333, "Ana", "ana@gmail.com", "4200-072");
        Cidadao c2 = new Cidadao(223344, "Berta", "berta@gmail.com", "4200-071");
        Senha s1 = new Senha(1, "A");
        Senha s2 = new Senha(1, "C");
        Senha s5 = new Senha(4, "C");
        r1.insertNewCidadao(c1);
        r1.insertNewCidadao(c2);
        r1.addCidadaoSenha(c2, s5);
        assertEquals(r1.getCidadaos(), instance.getCidadaos());

    }

    /**
     * Test of equals method, of class Reparticao.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new Reparticao("Porto", 12, "4200");
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Reparticao.
     */
    @Test
    public void testEquals1() {
        System.out.println("equals");
        Object obj = new Reparticao("Porto", 12, "4300");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of insertCidadaos method, of class Reparticao.
     */
    @Test
    public void testInsertCidadaos() {
        System.out.println("insertCidadaos");
        Cidadao c1 = new Cidadao(111222333, "Ana", "ana@gmail.com", "4200-072");
        Cidadao c2 = new Cidadao(223344, "Berta", "berta@gmail.com", "4200-071");
        Cidadao c3 = new Cidadao(584769, "Manuel", "manuel@gmail.com",
                "4715-357");
        Set<Cidadao> listaCidadaos = new HashSet<>();
        listaCidadaos.add(c1);
        listaCidadaos.add(c2);
        listaCidadaos.add(c3);
        Reparticao r = new Reparticao("Porto", 1234, "4200");
        r.insertCidadaos(listaCidadaos);
        assertEquals(r.getCidadaos().keySet(), listaCidadaos);
    }

    /**
     * Test of insertNewCidadao method, of class Reparticao.
     */
    @Test
    public void testInsertNewCidadao() {
        System.out.println("insertNewCidadao");
        Cidadao c1 = new Cidadao(111222333, "Ana", "ana@gmail.com", "4200-072");
        Reparticao r = new Reparticao("Porto", 1234, "4200");
        Reparticao r2 = new Reparticao("Porto", 1234, "4200");
        r.insertNewCidadao(c1);
        r2.insertNewCidadao(c1);
        assertEquals(r.getCidadaos().keySet(), r2.getCidadaos().keySet());
    }

}
