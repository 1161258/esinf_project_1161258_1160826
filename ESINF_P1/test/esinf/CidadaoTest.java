/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Vitor Melo
 */
public class CidadaoTest {
    
    public CidadaoTest() {
    }

    /**
     * Test of equals method, of class Cidadao.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new Cidadao(0, "Ana Maria", "anamaria@mail.com", "4200-123");
        Cidadao instance = new Cidadao(0, "Ana Maria", "anamaria@mail.com", "4200-123");;
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Cidadao.
     */
    @Test
    public void testEquals1() {
        System.out.println("equals");
        Object obj = new Cidadao(1, "Ana Maria", "anamaria@mail.com", "4200-123");
        Cidadao instance = new Cidadao(0, "Ana Maria", "anamaria@mail.com", "4200-123");;
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hashCode method, of class Cidadao.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Cidadao instance = new Cidadao(1, "Ana Maria", "anamaria@mail.com", "4200-123");;
        int expResult = 88;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }
    
}
