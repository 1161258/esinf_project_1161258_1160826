/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import java.util.Objects;

/**
 *
 * @author Filipe Ferreira
 * @author Vitor Melo
 */
public class Senha implements Comparable<Senha> {

    private final int nOrdem;
    private final String servico;

    /**
     *
     * @param nOrdem
     * @param serviço
     */
    public Senha(int nOrdem, String serviço) {
        this.nOrdem = nOrdem;
        this.servico = serviço;
    }

    /**
     * @return the nOrdem
     */
    public int getnOrdem() {
        return nOrdem;
    }

    /**
     * @return the servico
     */
    public String getServico() {
        return servico;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Senha pObj = (Senha) obj;
        return (pObj.nOrdem == nOrdem) && (pObj.servico.equals(servico));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.nOrdem;
        hash = 97 * hash + Objects.hashCode(this.servico);
        return hash;
    }

    @Override
    public int compareTo(Senha o) {
        if (nOrdem == o.nOrdem) {
            return servico.compareTo(o.servico);
        } else if (servico.equals(o.servico)) {
            return (nOrdem > o.nOrdem) ? 1 : (nOrdem < o.nOrdem) ? -1 : 0;
        }
        return servico.compareTo(o.servico);
    }

}
