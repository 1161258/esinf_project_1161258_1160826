/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Vitor Melo
 * @author Filipe Ferreira
 */
public class Reparticao {

    private String cidade;
    private final int nReparticao;
    private String codPostal;
    private Set<String> servicos;
    private final Map<Cidadao, Set<Senha>> cidadaos;

    /**
     * Construtor
     *
     * @param cidade
     * @param nReparticao
     * @param codPostal
     */
    public Reparticao(String cidade, int nReparticao, String codPostal) {
        this.cidade = cidade;
        this.nReparticao = nReparticao;
        this.codPostal = codPostal;
        this.servicos = new LinkedHashSet<>();
        this.cidadaos = new HashMap<>();
    }

    /**
     * Obter a cidade da repartição
     *
     * @return cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * Obter numero de reparticao
     *
     * @return nReparticao
     */
    public int getnReparticao() {
        return nReparticao;
    }

    /**
     * Obter o set de servicos disponiveis
     *
     * @return servicos
     */
    public Set<String> getServicos() {
        return servicos;
    }

    /**
     * Obter o codigo postal
     *
     * @return codigo postal
     */
    public String getCodPostal() {
        return codPostal;
    }

    /**
     * Obter o map cidadao de senhas
     *
     * @return cidadaos e senhas
     */
    public Map<Cidadao, Set<Senha>> getCidadaos() {
        return cidadaos;
    }

    /**
     * Definir cidade de reparticao
     *
     * @param cidade cidade
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * Definir servicos da reparticao
     *
     * @param servicos set de servicos
     */
    public void setServicos(Set<String> servicos) {
        this.servicos = servicos;
    }

    /**
     * Definir o codigo postal
     *
     * @param codPostal codigo postal
     */
    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    /**
     * Adiciona uma senha a um cidadao
     *
     * @param c cidadao
     * @param s senha
     */
    public void addCidadaoSenha(Cidadao c, Senha s) {
        for (Cidadao cidadao : cidadaos.keySet()) {
            if (cidadao.equals(c)) {
                Set<Senha> senhas = cidadaos.get(c);
                if (!senhas.contains(s)) {
                    senhas.add(s);
                }
            }
        }
    }

    /**
     * Verifica a igualdade entre reparticoes
     *
     * @param obj objeto
     * @return boolean consoante a igualdade
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Reparticao pObj = (Reparticao) obj;
        return (pObj.getCodPostal().equals(codPostal));
    }

    //--------------------Metodos do Projeto-----------------------------------
    /**
     * Atraves de um set insere os cidadaos do set no map
     *
     * @param listaCidadaos lista de cidadaos
     */
    public void insertCidadaos(Set<Cidadao> listaCidadaos) {
        Iterator<Cidadao> i = listaCidadaos.iterator();
        while (i.hasNext()) {
            cidadaos.put(i.next(), new LinkedHashSet<>());
        }
    }

    /**
     * Insere um unico cidadao na presente reparticao
     *
     * @param c cidadao
     */
    public void insertNewCidadao(Cidadao c) {
        cidadaos.put(c, new LinkedHashSet<>());
    }

    /**
     * Chama sequencialmente cada metodo para preencher o map
     *
     * @param cidadaos lista com a informacao dos cidadaos
     * @param senhas lista com a informacao das senhas
     */
    public void readAllCidadaosSenhas(List<String> cidadaos, List<String> senhas) {
        readCidadaos(cidadaos);
        readSenhas(senhas);
    }

    /**
     * Le de uma lista os cidadaos da reparticao
     *
     * @param cidadaos lista gerada com a informacao do ficheiro
     */
    private void readCidadaos(List<String> cidadaos) {
        Cidadao tempCidadao;
        for (String cidadao : cidadaos) {
            String[] s = cidadao.split(",");
            tempCidadao = new Cidadao(Integer.parseInt(s[1]), s[0], s[2], s[3]);
            if (Integer.parseInt(s[4]) == nReparticao
                    && !this.cidadaos.containsKey(tempCidadao)) {
                this.cidadaos.put(tempCidadao, new LinkedHashSet<>());
            }
        }
    }

    /**
     * Lê de uma lita as senhas da repartição
     *
     * @param senhas lista gerada com a informação do ficheiro texto
     */
    private void readSenhas(List<String> senhas) {
        Senha tempSenha;
        for (String senha : senhas) {
            String[] s = senha.split(",");
            for (Cidadao cidadao : cidadaos.keySet()) {
                if (s[0].equals(String.valueOf(cidadao.getnContribuinte()))) {
                    tempSenha = new Senha(Integer.parseInt(s[2]), s[1]);
                    cidadaos.get(cidadao).add(tempSenha);
                    break;
                }
            }
        }
    }

    /**
     * Alínea h)
     *
     * Permitir a um dado cidadao abandonar as filas para qual tirou senhas
     *
     * @param c cidadao para remover as senhas
     */
    public void abandonQueue(Cidadao c) {
        cidadaos.get(c).clear();
    }

}
