/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

/**
 *
 * @author Filipe Ferreira
 * @author Vitor Melo
 */
public class Cidadao {

    private final int nContribuinte;
    private final String nome;
    private String email;
    private String codPostal;

    /**
     *
     * @param nContribuinte
     * @param nome
     * @param email
     * @param codPostal
     */
    public Cidadao(int nContribuinte, String nome, String email,
            String codPostal) {
        this.nContribuinte = nContribuinte;
        this.nome = nome;
        this.email = email;
        this.codPostal = codPostal;
    }

    /**
     * @return the nContribuinte
     */
    public int getnContribuinte() {
        return nContribuinte;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return the codPostal
     */
    public String getCodPostal() {
        return codPostal;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @param codPostal the codPostal to set
     */
    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Cidadao pObj = (Cidadao) obj;
        return pObj.nContribuinte == nContribuinte;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + this.nContribuinte;
        return hash;
    }

   
}
