/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author Vitor Melo
 */
public class Financas {

    private final DoublyLinkedList<Reparticao> dll;

    /**
     * Construtor
     */
    public Financas() {
        dll = new DoublyLinkedList<>();
    }

    /**
     * Retorna a double linked list
     *
     * @return double linked list
     */
    public DoublyLinkedList<Reparticao> getDll() {
        return dll;
    }

    /**
     * Metódo para obter informação referente as repartições, bem como os
     * respetivos cidadaos e senhas
     *
     * @param reparticao Lista das repartições
     * @param cidadaos Lista dos cidadaos
     * @param senhas Lista das senhas
     */
    public void getFilesInfo(List<String> reparticao, List<String> cidadaos,
            List<String> senhas) {
        getReparticoes(reparticao);
        fillReparticoes(cidadaos, senhas);
    }

    /**
     * Obtem as reparicoes e introduz na doublelinkedlist
     *
     * @param reparticao lista com os dados das reparticoes
     */
    private void getReparticoes(List<String> reparticao) {
        Reparticao rep;
        for (String r : reparticao) {
            String[] s = r.split(",");
            rep = new Reparticao(s[0], Integer.parseInt(s[1]), s[2]);
            Set<String> set = new HashSet<>();
            for (int i = 3; i < s.length; i++) {
                set.add(s[i]);
            }
            rep.setServicos(set);
            dll.addLast(rep);
        }
    }

    /**
     * Metodo que para cada reparticao as preenche com os respetivos cidadaos e
     * senhas
     *
     * @param cidadaos lista dos dados dos cidadaos
     * @param senhas lista dos dados das senhas
     */
    private void fillReparticoes(List<String> cidadaos, List<String> senhas) {
        ListIterator<Reparticao> iterator = dll.listIterator();
        while (iterator.hasNext()) {
            iterator.next().readAllCidadaosSenhas(cidadaos, senhas);
        }
    }

    /**
     * Remover uma repartição passando todos os cidadaos para a reparticao mais
     * proxima
     *
     * Alinea c
     *
     * @param codPostal codigo postal da reparticao para remover
     */
    public void removeReparticao(String codPostal) {
        Reparticao reparticao = findReparticaoToRemove(codPostal);
        Reparticao reparticaoProxima = findClosestReparticao(codPostal);
        if (reparticao != null) {
            Set<Cidadao> listaCidadaos = reparticao.getCidadaos().keySet();
            reparticaoProxima.insertCidadaos(listaCidadaos);
        }
    }

    /**
     * Tenta encontrar a reparticao na lista, remove-a da lista e retorna-a
     *
     * Alinea c
     *
     * @param codPostal codigo postal para encontrar
     * @return null se nao encontrar ou o objeto se o encontrar
     */
    private Reparticao findReparticaoToRemove(String codPostal) {
        ListIterator<Reparticao> iterator = dll.listIterator();
        while (iterator.hasNext()) {
            Reparticao tempR = iterator.next();
            if (tempR.getCodPostal().equals(codPostal)) {
                iterator.remove();
                return tempR;
            }
        }
        return null;
    }

    /**
     * Retorna a reparticao mais proxima da lista para transferencia dos
     * cidadaos
     *
     * Alinea c
     *
     * @param codPostal codigo postal para encontrar o mais proximo
     * @return reparticao mais proxima da removida
     */
    private Reparticao findClosestReparticao(String codPostal) {
        int closeness = -1;
        int diferenca;
        int codPostalInt = Integer.parseInt(codPostal);
        Reparticao reparticaoProxima = null;
        Reparticao reparticaoTemp;
        ListIterator<Reparticao> iterator = dll.listIterator();
        while (iterator.hasNext()) {
            reparticaoTemp = iterator.next();
            int codPostalProx = Integer.parseInt(reparticaoTemp.getCodPostal());

            diferenca = (codPostalInt > codPostalProx ?
                    codPostalInt - codPostalProx :
                    codPostalProx - codPostalInt);

            if ((closeness == -1) || (closeness > diferenca)) {
                closeness = diferenca;
                reparticaoProxima = reparticaoTemp;
            }

        }
        return reparticaoProxima;
    }

    /**
     * Inserir novos cidadãos,não permitindo o registo de cidadãos repetidos.
     * Cada cidadão está associado a uma só repartição.
     *
     * Alinea e
     *
     * @param cidadaos
     */
    public void addNewCidadaos(List<String> cidadaos) {
        for (String cidadao : cidadaos) {

            String[] s = cidadao.split(",");
            String[] codPostal = s[3].split("-");

            Cidadao c = new Cidadao(Integer.parseInt(s[1]), s[0], s[2], s[3]);
            Reparticao r = findReparticao(codPostal[0]);

            if (r != null && !existsCidadaoInList(c)) {
                r.insertNewCidadao(c);
            }
        }
    }

    /**
     * Procura e retorna a reparticao que tem o mesmo codigo postal
     *
     * Alinea E e
     *
     * @param codPostal codigo postal para procurar
     * @return reparticao correspondente ao codigo postal
     */
    private Reparticao findReparticao(String codPostal) {
        ListIterator<Reparticao> iterator = dll.listIterator();
        while (iterator.hasNext()) {
            Reparticao t = iterator.next();
            if (t.getCodPostal().equals(codPostal)) {
                return t;
            }
        }
        return null;
    }

    /**
     * Verifica se o cidadao existe em alguma reparticao
     *
     * Alinea e
     *
     * @param c cidadao a procurar
     * @return falso se nao encontrar o cidadao em nenhuma reparticao
     */
    private boolean existsCidadaoInList(Cidadao c) {
        ListIterator<Reparticao> iterator = dll.listIterator();
        while (iterator.hasNext()) {
            Reparticao r = iterator.next();
            if (r.getCidadaos().keySet().contains(c)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Adiciona uma nova repartição, verifica se o codigo de postal não é igual 
     * ao de outra repartição, adiciona os cidadaos com o mesmo codigo potal 
     * (primeiros 4 digitos)
     * 
     * Alinea b
     * 
     * @param reparticao
     * @param cidadaos
     */
    public void addNewReparticao(List<String> reparticao, List<String> cidadaos) {
        Reparticao newRep;
        for (String r : reparticao) {
            String[] s = r.split(",");
            newRep = new Reparticao(s[0], Integer.parseInt(s[1]), s[2]);
            if (!existsReparticao(newRep)) {

                Set<String> set = new HashSet<>();
                for (int i = 3; i < s.length; i++) {
                    set.add(s[i]);
                }
                newRep.setServicos(set);

                for (String cidadao : cidadaos) {
                    String[] splitAll = cidadao.split(",");
                    Cidadao tempCidadao
                            = new Cidadao(Integer.parseInt(s[1]), s[0], s[2],
                                    s[3]);
                    String[] splitCP = splitAll[3].split("-");
                    String cP = splitCP[0];
                    if (newRep.getCodPostal().equals(cP)) {
                        newRep.insertNewCidadao(tempCidadao);
                    }
                }
                dll.addLast(newRep);
            }
        }
    }

    /**
     * Verifica se existe uma reparticao igual na lista
     *
     * Alinea b
     * 
     * @param r
     * @return
     */
    private boolean existsReparticao(Reparticao r) {
        ListIterator<Reparticao> iterator = dll.listIterator();
        while (iterator.hasNext()) {
            Reparticao next = iterator.next();
            if (next.equals(r)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Cria um map com as reparticoes e o servico correspondente com mais
     * frequencia
     *
     * Alínea g
     *
     * @return
     */
    public Map<Reparticao, String> getServicosMaisProcurados() {
        ListIterator<Reparticao> iterator = dll.listIterator();
        Map<Reparticao, String> maisFreq = new HashMap<>();
        while (iterator.hasNext()) {
            Reparticao r = iterator.next();
            maisFreq.put(r, findServicoMaisProcurado(r));
        }
        return maisFreq;
    }

    /**
     * Encontra o servico mais procurado numa determinada reparticao
     *
     * Alinea G
     *
     * @param r Reparticao
     * @return servico mais procurado na reparticao com o codigo postal
     */
    private String findServicoMaisProcurado(Reparticao r) {
        return getServicoMaisFrequente(getFrequenciaSenhas(r));
    }

    /**
     * Obtem a frequencia dos servicos numa reparticao
     *
     * @param r reparticao
     * @return
     */
    private HashMap<String, Integer> getFrequenciaSenhas(Reparticao r) {
        HashMap<String, Integer> servicosFreq = new HashMap<>();
        Map<Cidadao, Set<Senha>> cidadaos = r.getCidadaos();
        for (Cidadao cidadao : cidadaos.keySet()) {
            Iterator<Senha> i = cidadaos.get(cidadao).iterator();
            while (i.hasNext()) {
                Senha next = i.next();
                String letraSenha = next.getServico();
                if (servicosFreq.containsKey(letraSenha)) {
                    int numSenhas = servicosFreq.get(letraSenha);
                    servicosFreq.replace(letraSenha, numSenhas, numSenhas + 1);
                } else {
                    servicosFreq.put(letraSenha, 0);
                }
            }
        }
        return servicosFreq;
    }

    /**
     * Verifica qual foi o servico mais frequente no hashmap das frequencias dos
     * servicos
     *
     * @param servicosFreq frequencia dos servicos
     * @return servico com maior frequencia
     */
    private String getServicoMaisFrequente(HashMap<String, Integer> servicosFreq) {
        int highestFreq = 0;
        String highestFreqServ = null;
        for (String string : servicosFreq.keySet()) {
            if (servicosFreq.get(string) > highestFreq) {
                highestFreq = servicosFreq.get(string);
                highestFreqServ = string;
            }
        }
        return highestFreqServ;
    }

    /**
     * Obtem o numero e cidade de uma reparticao e todos os seus clientes
     *
     * Alinea D
     *
     * @return map com a informacao da cidade e numero juntamente com os seus
     * respetivos numeros de contribuinte
     */
    public Map<String, Set<Integer>> getReparticaoData() {
        Map<String, Set<Integer>> reparticoesClientes = new HashMap<>();
        ListIterator<Reparticao> iterator = dll.listIterator();
        while (iterator.hasNext()) {
            Reparticao r = iterator.next();
            Set<Integer> nContribuintes = new HashSet<>();
            for (Cidadao cidadao : r.getCidadaos().keySet()) {
                nContribuintes.add(cidadao.getnContribuinte());
            }
            reparticoesClientes.put(String.format("%d - %s", r.getnReparticao(),
                    r.getCidade()), nContribuintes);
        }
        return reparticoesClientes;
    }

    /**
     * Alinea F
     *
     * @param r
     * @param hours
     * @param minutes
     * @return
     */
    public Map<String, LinkedHashSet<Cidadao>> getRepaticaoUsage(Reparticao r,
            int hours, int minutes) {
        int nOrdemAtual = 1;
        int nServicos = r.getServicos().size();
        int horaEntrada = 9;
        int horaSaida = 15;
        int minutoEntrada = 0;
        int minutoSaida = 30;
        int tempoAtendimentoMin = 10;
        int tempoEntrada = horaEntrada * 60 + minutoEntrada;
        int tempoSaida = horaSaida * 60 + minutoSaida;
        int tempoFinal = (hours * 60 + minutes) + tempoEntrada;

        tempoFinal = tempoFinal > tempoSaida ? tempoSaida : tempoFinal;

        TreeMap<Cidadao, Set<Senha>> mcp = new TreeMap<>();
        mcp.putAll(r.getCidadaos());

        ArrayList<Cidadao> cidadaos = new ArrayList<>();
        ArrayList<Senha> senhasAtuais = new ArrayList<>();

        //para saber a ordem dos cidadaos
        for (Cidadao cidadao : mcp.keySet()) {
            cidadaos.add(cidadao);
        }
        return null;

    }

}
