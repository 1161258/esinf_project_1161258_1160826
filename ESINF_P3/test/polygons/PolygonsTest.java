/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polygons;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import libs.tree.AVL;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Vitor Melo
 */
public class PolygonsTest {

    private Polygons p;
    
    public PolygonsTest() throws IOException {
        List<String> uni = Files.lines(Paths.get(
                "poligonos_prefixo_unidades.txt"))
                .collect(Collectors.toList());
        List<String> dez = Files.lines(Paths.
                get("poligonos_prefixo_dezenas.txt"))
                .collect(Collectors.toList());
        List<String> cen = Files.lines(Paths.get(
                "poligonos_prefixo_centenas.txt"))
                .collect(Collectors.toList());

        p = new Polygons(uni, dez, cen);
    }

    /**
     * Test of polygonName method, of class Polygons.
     */
    @Test
    public void testPolygonName() {
        System.out.println("polygonName");
        int number = 524;
        String expResult = "pentahectaicositetragon";
        String result = p.polygonName(number);
        assertEquals(expResult, result);
    }

    /**
     * Test of polygonName method, of class Polygons.
     */
    @Test
    public void testPolygonName1() {
        System.out.println("polygonName");
        int number = 845;
        String expResult = "octahectatetracontapentagon";
        String result = p.polygonName(number);
        assertEquals(expResult, result);
    }

    /**
     * Test of polygonName method, of class Polygons.
     */
    @Test
    public void testPolygonName2() {
        System.out.println("polygonName");
        int number = 900;
        String expResult = "enneahectagon";
        String result = p.polygonName(number);
        assertEquals(expResult, result);
    }

    /**
     * Test of getTreeAllPolygons method, of class Polygons.
     */
    @Test
    public void testGetTreeAllPolygons() throws IOException {
        System.out.println("getTreeAllPolygons");
        List<String> ladosNomes = Files.lines(Paths.get(
                "teste_lados_nome.txt"))
                .collect(Collectors.toList());
        AVL<PolyDescription> exp = new AVL<>();
        for (int i = 0; i < ladosNomes.size(); i++) {
            String s = ladosNomes.get(i);
            if (!s.isEmpty()) {
                String[] linhaSplit = s.split(";");
                exp.insert(new PolyDescription(Integer.parseInt(linhaSplit[0]),
                        linhaSplit[1]));
            }
        }
        AVL<PolyDescription> result = p.getTreeAllPolygons();
        assertEquals(exp.inOrder(), result.inOrder());
       
    }

    /**
     * Test of getNumberOfSides method, of class Polygons.
     */
    @Test
    public void testGetNumberOfSides() {
        System.out.println("getNumberOfSides");
        String des = "heptahectatetracontahenagon";
        int expResult = 741;
        int result = p.getNumberOfSides(des);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNamesRange method, of class Polygons.
     * @throws java.io.IOException
     */
    @Test
    public void testGetNamesRange() throws IOException {
        System.out.println("getNamesRange");
        int min = 1;
        int max = 999;
        List<String> expResult = new LinkedList<>();
        List<String> ladosNomes = Files.lines(Paths.get(
                "teste_lados_nome.txt"))
                .collect(Collectors.toList());
        for (int i = 0; i < ladosNomes.size(); i++) {
            String s = ladosNomes.get(i);
            if (!s.isEmpty()) {
                String[] linhaSplit = s.split(";");
                expResult.add(linhaSplit[1]);
            }
        }
        List<String> result = p.getNamesRange(min, max);
        Collections.reverse(result);
        assertEquals(expResult, result);
    }

    /**
     * Test of lowestCommonAncestor method, of class Polygons.
     */
    @Test
    public void testLowestCommonAncestor() {
        System.out.println("lowestCommonAncestor");
        String nome1 = "tetracontahexagon";
        String nome2 = "trihectaoctacontadigon";
        PolyDescription expResult = new PolyDescription(256, "dihectapentacontahexagon");
        PolyDescription result = p.lowestCommonAncestor(nome1, nome2);
        assertEquals(expResult, result);
    }

}
