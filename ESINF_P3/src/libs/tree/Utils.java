
package libs.tree;

import java.util.List;

/**
 *
 * @author DEI-ESINF
 */
public class Utils {
    public static <E extends Comparable<E>> Iterable<E> sortByBST(List<E> listUnsorted){
        BST<E> bs= new BST();
        for (E e : listUnsorted) {
            bs.insert(e);
        }
        return bs.inOrder();
    }    
}
