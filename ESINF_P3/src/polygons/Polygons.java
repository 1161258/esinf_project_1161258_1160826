/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polygons;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import libs.tree.AVL;

/**
 *
 * @author Filipe Ferreira
 * @author Vitor Melo
 */
public class Polygons {

    private AVL<PolyDescription> treeUnidades;

    private AVL<PolyDescription> treeDezenas;

    private AVL<PolyDescription> treeCentenas;

    public Polygons(List<String> uni, List<String> dez, List<String> cen) {
        createTreeUnidades(uni);
        createTreeDezenas(dez);
        createTreeCentenas(cen);
    }

    /**
     * Alínea A Criação da árvore balanceada das Unidades
     *
     * @param ficheiro
     */
    private void createTreeUnidades(List<String> ficheiro) {
        treeUnidades = new AVL<>();
        for (int i = 0; i < ficheiro.size(); i++) {
            String s = ficheiro.get(i);
            if (!s.isEmpty()) {
                String[] linhaSplit = s.split(";");
                treeUnidades.insert(new PolyDescription(Integer.parseInt(
                        linhaSplit[0]),
                        linhaSplit[1]));
            }
        }
    }

    /**
     * Alínea A Criação da árvore balanceada das Dezenas
     *
     * @param ficheiro
     */
    private void createTreeDezenas(List<String> ficheiro) {
        treeDezenas = new AVL<>();
        for (int i = 0; i < ficheiro.size(); i++) {
            String s = ficheiro.get(i);
            if (!s.isEmpty()) {
                String[] linhaSplit = s.split(";");
                treeDezenas.insert(new PolyDescription(Integer.parseInt(
                        linhaSplit[0]),
                        linhaSplit[1]));
            }
        }
    }

    /**
     * Alínea A Criação da árvore balanceada das Centenas
     *
     * @param ficheiro
     */
    private void createTreeCentenas(List<String> ficheiro) {
        treeCentenas = new AVL<>();
        for (int i = 0; i < ficheiro.size(); i++) {
            String s = ficheiro.get(i);
            if (!s.isEmpty()) {
                String[] linhaSplit = s.split(";");
                treeCentenas.insert(new PolyDescription(Integer.parseInt(
                        linhaSplit[0]),
                        linhaSplit[1]));
            }
        }
    }

    /**
     * Alinea B
     *
     * Obtem o nome do poligono dado um numero
     *
     * @param number
     * @return
     */
    public String polygonName(int number) {
        if (number >= 1 && number <= 999) {
            List<String> name = new LinkedList<>();
            int remain = number % 100;
            if (remain >= 10 && remain <= 29) {
                appendNameDezenas(name, remain);
                number -= remain;
                appendNameCentenas(name, number);
            } else {
                appendAll(name, number);
            }
            Collections.reverse(name);
            String polygonName = "";
            for (String string : name) {
                polygonName = polygonName.concat(string);
            }
            polygonName = polygonName.concat("gon");
            return polygonName;
        }
        return null;

    }

    /**
     * Alinea B
     *
     * Disseca o numero e constrói o nome
     *
     * @param name
     * @param number
     */
    private void appendAll(List<String> name, int number) {
        int mod = 10;
        while (number != 0) {
            int remain = number % mod;
            switch (mod) {
                case 10:
                    if (remain != 0) {
                        appendNameUnidades(name, remain);
                    }
                    break;
                case 100:
                    if (remain != 0) {
                        appendNameDezenas(name, remain);
                    }
                    break;
                case 1000:
                    appendNameCentenas(name, remain);
                    break;
                default:
                    break;
            }
            number -= remain;
            mod *= 10;
        }
    }

    /**
     * Alinea B
     *
     * Obtem o nome das dezenas
     *
     * @param name
     * @param number
     */
    private void appendNameDezenas(List<String> name, int number) {
        Iterable<PolyDescription> it = treeDezenas.inOrder();
        for (PolyDescription names : it) {
            if (names.getNum() == number) {
                name.add(names.getDesignacao());
            }
        }
    }

    /**
     * Alinea B
     *
     * Obtem o nome das centenas
     *
     * @param name
     * @param number
     */
    private void appendNameCentenas(List<String> name, int number) {
        Iterable<PolyDescription> it = treeCentenas.inOrder();
        for (PolyDescription names : it) {
            if (names.getNum() == number) {
                name.add(names.getDesignacao());
            }
        }
    }

    /**
     * Alinea B
     *
     * Obtem o nome das unidades
     *
     * @param name
     * @param number
     */
    private void appendNameUnidades(List<String> name, int number) {
        Iterable<PolyDescription> it = treeUnidades.inOrder();
        for (PolyDescription names : it) {
            if (names.getNum() == number) {
                name.add(names.getDesignacao());
            }
        }
    }

    /**
     * Alínea C - Constroi uma nova árvore com todos os poligonos possiveis,
     * entre 1 e 999
     *
     * @return Tree com todos os poligonos
     */
    public AVL<PolyDescription> getTreeAllPolygons() {
        AVL<PolyDescription> treeAllPolygons = new AVL<>();

        for (int i = 1; i < 1000; i++) {
            treeAllPolygons.insert(new PolyDescription(i, polygonName(i)));
        }

        return treeAllPolygons;
    }

    /**
     * Alínea D - Através um nome passado por parâmetro, obtem o num de lados
     *
     * @param des designação do poligono
     * @return numero de lados
     */
    public int getNumberOfSides(String des) {
        int sides = 0;
        AVL<PolyDescription> all = getTreeAllPolygons();
        for (PolyDescription n : all.inOrder()) {
            if (des.equals(n.getDesignacao())) {
                sides = n.getNum();
            }
        }
        return sides;
    }

    /**
     * Alínea E - Dado um intervalo, obtem a lista de nomes dos poligonos dentro
     * do intervalo, pela ordem inversa
     *
     * @param min valor inferior do intervalo
     * @param max valor superior do intervalo
     * @return
     */
    public List<String> getNamesRange(int min, int max) {
        List<String> names = new ArrayList<>();
        AVL<PolyDescription> treeAllPolygons = new AVL<>();
        for (int i = min; i < max + 1; i++) {
            treeAllPolygons.insert(new PolyDescription(i, polygonName(i)));
        }
        for (PolyDescription n : treeAllPolygons.inOrder()) {
            names.add(n.getDesignacao());
        }
        Collections.reverse(names);
        return names;
    }

    /**
     * Alínea F - Encontra o ancestral comum entre dois elementos da árvore
     *
     * @param nome1 nome de um poligono
     * @param nome2 nome de um poligono
     * @return menor ancestral comum
     */
    public PolyDescription lowestCommonAncestor(String nome1, String nome2) {
        AVL<PolyDescription> treeAllPolygons = getTreeAllPolygons();
        List<PolyDescription> list = new LinkedList<>();
        for (PolyDescription p : treeAllPolygons.inOrder()) {
            if (p.getDesignacao().equals(nome1) || p.getDesignacao().equals(
                    nome2)) {
                list.add(p);
            }
        }
        PolyDescription lowestCommon = null;
        if (list.size() == 2) {
            lowestCommon = treeAllPolygons.lowestCommonAncestor(list.get(0),
                    list.get(1));
        }

        return lowestCommon;
    }

}
