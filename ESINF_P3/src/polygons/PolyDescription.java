/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polygons;

import java.util.Objects;

/**
 *
 * @author Filipe Ferreira
 * @author Vitor Melo
 */
public class PolyDescription implements Comparable<PolyDescription>{
    
    /**
     * Atribulo número do poligono
     */
    private int num;
    
    /**
     * Atributo designação do poligono.
     */
    private String designacao;
    
    /**
     * Construtor
     * 
     * @param num
     * @param designacao 
     */
    public PolyDescription(int num, String designacao){
        this.num = num;
        this.designacao = designacao;
    }

    /**
     * @return the num
     */
    public int getNum() {
        return num;
    }

    /**
     * @return the designacao
     */
    public String getDesignacao() {
        return designacao;
    }

    /**
     * @param num the num to set
     */
    public void setNum(int num) {
        this.num = num;
    }

    /**
     * @param designacao the designacao to set
     */
    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        PolyDescription pObj = (PolyDescription) obj;
        return pObj.num == num && pObj.designacao.equals(designacao);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + this.num;
        hash = 37 * hash + Objects.hashCode(this.designacao);
        return hash;
    }

    @Override
    public int compareTo(PolyDescription obj) {
        return num > obj.getNum() ? 1 : num < obj.getNum() ? -1 : 0;
        
    }
    
}
